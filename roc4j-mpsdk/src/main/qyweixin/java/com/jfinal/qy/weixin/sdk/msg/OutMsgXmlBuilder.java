
package com.jfinal.qy.weixin.sdk.msg;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.qy.weixin.sdk.msg.out.OutImageMsg;
import com.jfinal.qy.weixin.sdk.msg.out.OutMsg;
import com.jfinal.qy.weixin.sdk.msg.out.OutNewsMsg;
import com.jfinal.qy.weixin.sdk.msg.out.OutTextMsg;
import com.jfinal.qy.weixin.sdk.msg.out.OutVideoMsg;
import com.jfinal.qy.weixin.sdk.msg.out.OutVoiceMsg;

import org.beetl.core.BeetlKit;

/**
 * 利用 FreeMarker 动态生成 OutMsg xml 内容 
 */
public class OutMsgXmlBuilder {

	private static Map<String, String> templateMap = new HashMap<>();

	static{
		// text 文本消息
		templateMap.put(OutTextMsg.class.getSimpleName(), OutTextMsg.TEMPLATE);
		// news 图文消息
		templateMap.put(OutNewsMsg.class.getSimpleName(), OutNewsMsg.TEMPLATE);
		// image 图片消息
		templateMap.put(OutImageMsg.class.getSimpleName(), OutImageMsg.TEMPLATE);
		//voice 语音消息
		templateMap.put(OutVoiceMsg.class.getSimpleName(), OutVoiceMsg.TEMPLATE);
		// video 视频消息
		templateMap.put(OutVideoMsg.class.getSimpleName(), OutVideoMsg.TEMPLATE);
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static String build(OutMsg outMsg) {
		if (outMsg == null)
			throw new IllegalArgumentException("参数 OutMsg 不能为 null");

		if(!templateMap.containsKey(outMsg.getClass().getSimpleName())){
			throw new RuntimeException("不支持构建 " + outMsg.getClass().getSimpleName()+  " 消息对象");
		}
		
		Map root = new HashMap();
		// 供 OutMsg 里的 TEMPLATE 使用
		root.put("__msg", outMsg);
		try {
			return BeetlKit.render(templateMap.get(outMsg.getClass().getSimpleName()), root);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args){
		OutTextMsg msg = new OutTextMsg();
		msg.setContent("1");
		msg.setCreateTime(1);
		msg.setFromUserName("f");
		msg.setToUserName("t");
		System.out.println(OutMsgXmlBuilder.build(msg));
	}

}







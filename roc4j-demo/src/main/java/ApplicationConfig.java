import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.handler.SessionIdHandler;
import com.jfinal.ext.handler.UrlSkipHandler;
import com.jfinal.ext.interceptor.ExceptionInterceptor;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.ext.interceptor.syslog.LogConfig;
import com.jfinal.ext.interceptor.syslog.SysLog;
import com.jfinal.ext.interceptor.syslog.SysLogInterceptor;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.ext.session.SessionDao;
import com.jfinal.ext.session.SessionManager;
import com.jfinal.ext.session.impl.CacheSessionManager;
import com.jfinal.ext.session.impl.DBSessionDao;
import com.jfinal.ext.session.plugin.SessionManagerPlugin;
import com.jfinal.i18n.I18nInterceptor;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.config.ConfigKit;
import com.jfinal.plugin.config.ConfigPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.plugin.quartz.QuartzPlugin;
import com.jfinal.plugin.sqlmgr.SqlManagerPlugin;
import com.jfinal.websocket.MessageFactory;
import com.jfinal.weixin.sdk.api.ApiConfigKit;
import me.roczh.roc4j.demo.core.MyLogProccesor;
import me.roczh.roc4j.demo.model._MappingKit;
import me.roczh.roc4j.demo.msg.TestMessage;
import me.roczh.roc4j.demo.view.TemplateKit;

/**
 * Created by peng on 2016/3/17.
 */
public class ApplicationConfig extends JFinalConfig {
    public void configConstant(Constants me) {
        ConfigPlugin.setSuffix("conf");
        new ConfigPlugin(".*.conf").reload(true).start();
        me.setDevMode(ConfigKit.getBoolean("devMode", true));
        TemplateKit.regiseter();
        me.setError404View("/common/404.html");
        me.setError500View("/common/500.html");
        me.setBaseUploadPath("../../../target/upload");
        me.setBaseDownloadPath("../../../target/upload");
/*        // ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
        ApiConfigKit.setDevMode(me.getDevMode());*/
        // ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
        ApiConfigKit.setDevMode(me.getDevMode());
    }

    public void configRoute(Routes me) {
        me.add(new AutoBindRoutes());
/*        me.add(new QyWeixinRoutes());
        me.add(new WeixinRoutes());*/
    }

    public void configPlugin(Plugins me) {
        DruidPlugin druidPlugin = new DruidPlugin(ConfigKit.getStr("jdbcUrl"),
                ConfigKit.getStr("user"), ConfigKit.getStr("password"));
        me.add(druidPlugin);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        me.add(arp);
        _MappingKit.mapping(arp);
        arp.addMapping("TBL_SYSLOG", "ID", SysLog.class);
        arp.setDialect(new OracleDialect());
        arp.setTransactionLevel(8);
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
        arp.setShowSql(true);
        me.add(new EhCachePlugin());
        me.add(new QuartzPlugin());
        me.add(new SqlManagerPlugin());
        //local session存储器
        SessionDao sessionDao = new DBSessionDao();
        SessionManager sessionManager = new CacheSessionManager(sessionDao);
        sessionManager.setSessionTimeout(10000);
        SessionManagerPlugin sessionManagerPluginPlugin = new SessionManagerPlugin(sessionManager);
        me.add(sessionManagerPluginPlugin);
    }

    public void configInterceptor(Interceptors me) {
        me.addGlobalActionInterceptor(new SessionInViewInterceptor());
        me.addGlobalActionInterceptor(new I18nInterceptor("_locale", "i18n", false));
       // me.addGlobalActionInterceptor(new AuthInterceptor());
        me.add(new ExceptionInterceptor().addMapping(IllegalArgumentException.class, "/common/exception1.html").setDefault("/common/500.html"));
        SysLogInterceptor sysLogInterceptor = new SysLogInterceptor();
        me.add(sysLogInterceptor);
        sysLogInterceptor.addConfig("/blog", new LogConfig("访问博客").addPara("user", "博主")).setLogProcesser(new MyLogProccesor());
        sysLogInterceptor.addConfig("/m/blog", new LogConfig("请求博客数据").addPara("user", "博主")).setLogProcesser(new MyLogProccesor());

    }

    public void configHandler(Handlers me) {
        me.add(new SessionIdHandler());
        me.add(new UrlSkipHandler("/(assets|4msg|4console)/[\\s\\S]*", false));
        me.add(new ContextPathHandler());
    }

    @Override
    public void afterJFinalStart() {
        super.afterJFinalStart();
        MessageFactory.registerMessage(TestMessage.MSG_CODE, TestMessage.class);
        LogKit.info("afterJFinalStart 启动操作日志入库线程");
        MyLogProccesor.startSaveDBThread(5);
    }

    @Override
    public void beforeJFinalStop() {
        super.beforeJFinalStop();
        LogKit.info("beforeJFinalStop 释放日志入库线程");
        MyLogProccesor.setThreadRun(false);
    }

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 8080, "/demo", 5);
    }
}

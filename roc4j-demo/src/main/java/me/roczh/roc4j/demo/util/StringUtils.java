package me.roczh.roc4j.demo.util;

import com.jfinal.kit.LogKit;
import com.jfinal.plugin.config.ConfigKit;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.Key;
import java.text.MessageFormat;
import java.util.Map;

/**
 * Created by peng on 2016/5/3.
 */
public class StringUtils {

    /**
     * 字符编码
     */
    public final static String encoding = "UTF-8";

    private static final String _pwdEncrypt_EXTRA_CODE = "roc4j";
    private static final Key key;

    static{
        try {
            DESKeySpec objDesKeySpec = new DESKeySpec(ConfigKit.getStr("config.secret.key","12345678901234567890123456789012").getBytes("UTF-8"));
            SecretKeyFactory objKeyFactory = SecretKeyFactory.getInstance("DES");
            key = objKeyFactory.generateSecret(objDesKeySpec);
        } catch (Exception e) {
            LogKit.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * MD5 密码加密
     * @param password
     * @return
     */
    public static String encryptPasswd(String password){
        return DigestUtils.md5Hex(DigestUtils.md5Hex(password + StringUtils._pwdEncrypt_EXTRA_CODE));
    }

    /**
     * 对字符串进行DES加密
     *
     * @param plaintext
     * @return BASE64编码的加密字符串
     */
    public static String encryptString(String plaintext) {
        byte[] bytes = plaintext.getBytes();
        try {
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encryptStrBytes = cipher.doFinal(bytes);
            return Base64.encodeBase64String(encryptStrBytes);
        } catch (Exception e) {
            LogKit.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 对BASE64编码的加密字符串进行解密
     *
     * @param ciphertext
     * @return 解密后的字符串
     */
    public static String decryptString(String ciphertext) {
        try {
            byte[] bytes = Base64.decodeBase64(ciphertext);
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            bytes = cipher.doFinal(bytes);
            return new String(bytes);
        } catch (Exception e) {
            LogKit.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Url Base64编码
     *
     * @param data  待编码数据
     * @return String 编码数据
     * @throws Exception
     */
    public static String encode(String data) throws Exception {
        // 执行编码
        byte[] b = Base64.encodeBase64URLSafe(data.getBytes(encoding));
        return new String(b, encoding);
    }

    /**
     * Url Base64解码
     *
     * @param data
     *            待解码数据
     * @return String 解码数据
     * @throws Exception
     */
    public static String decode(String data) throws Exception {
        // 执行解码
        byte[] b = Base64.decodeBase64(data.getBytes(encoding));
        return new String(b, encoding);
    }

    /**
     * 将字符串中特定模式的字符转换成map中对应的值
     *
     * use: format("my name is ${name}, and i like ${like}!", {"name":"L.cm", "like": "Java"})
     *
     * @param s		需要转换的字符串
     * @param map	转换所需的键值对集合
     * @return		转换后的字符串
     */
    public static String format(String s, Map<String, String> map) {
        StringBuilder sb = new StringBuilder((int)(s.length() * 1.5));
        int cursor = 0;
        for (int start, end; (start = s.indexOf("${", cursor)) != -1 && (end = s.indexOf('}', start)) != -1;) {
            sb.append(s.substring(cursor, start));
            String key = s.substring(start + 2, end);
            sb.append(map.get(key.trim()));
            cursor = end + 1;
        }
        sb.append(s.substring(cursor, s.length()));
        return sb.toString();
    }



    /**
     * 字符串格式化
     *
     * use: format("my name is {0}, and i like {1}!", "L.cm", "java")
     *
     * int long use {0,number,#}
     *
     * @param s
     * @param args
     * @return 转换后的字符串
     */
    public static String format(String s, Object... args) {
        return MessageFormat.format(s, args);
    }

    /**
     * 替换某个字符
     * @param str
     * @param regex
     * @param args
     * @return
     */
    public static String replace(String str,String regex,String... args){
        int length = args.length;
        for (int i = 0; i < length; i++) {
            str=str.replaceFirst(regex, args[i]);
        }
        return str;
    }
}

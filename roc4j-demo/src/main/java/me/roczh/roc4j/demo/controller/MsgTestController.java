package me.roczh.roc4j.demo.controller;

import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.LogKit;
import com.jfinal.websocket.MessageFactory;
import com.jfinal.websocket.MsgSrv;
import me.roczh.roc4j.demo.msg.TestMessage;

/**
 * Created by peng on 2016/3/17.
 */
@ControllerBind(controllerKey = "/test/msg")
public class MsgTestController extends Controller {

    public void index(){
        setAttr("uid", getPara("uid"));
        setSessionAttr(MsgSrv.KEY_CURRENT_USER_ID, getPara("uid"));
        LogKit.debug(MessageFactory.SUPPORED_MSG_CODES.toString());
        render("/test/msg.html");
    }

    public void send(){
        TestMessage testMessage = TestMessage.create("这是一条单向消息的内容!");
        testMessage.setTouser("1");
        MsgSrv.send(testMessage);
        renderText("send complete!");
    }

    public void sendall(){
        TestMessage testMessage = TestMessage.create("这是一条群发消息的内容!");
        MsgSrv.sendAll(testMessage, false);
        renderText("send complete!");
    }
}
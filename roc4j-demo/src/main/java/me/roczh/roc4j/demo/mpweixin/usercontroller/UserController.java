package me.roczh.roc4j.demo.mpweixin.usercontroller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jfinal.ext.render.excel.PoiRender;
import com.jfinal.plugin.config.ConfigKit;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.weixin.sdk.api.AccessTokenApi;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.UserApi;
import com.jfinal.weixin.sdk.jfinal.ApiController;

/**
 * @author Javen
 * @Email javenlife@126.com
 */
public class UserController extends ApiController{
	private static final Logger log =  Logger.getLogger(UserController.class);
	String next_openid=null;
	private static String batchGetUserInfo = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=";
	/**
	 * 如果要支持多公众账号，只需要在此返回各个公众号对应的 ApiConfig 对象即可 可以通过在请求 url 中挂参数来动态从数据库中获取
	 * ApiConfig 属性值
	 */
	public ApiConfig getApiConfig() {
		ApiConfig ac = new ApiConfig();

		// 配置微信 API 相关常量
		ac.setToken(ConfigKit.getStr("token"));
		ac.setAppId(ConfigKit.getStr("appId"));
		ac.setAppSecret(ConfigKit.getStr("appSecret"));

		/**
		 * 是否对消息进行加密，对应于微信平台的消息加解密方式： 1：true进行加密且必须配置 encodingAesKey
		 * 2：false采用明文模式，同时也支持混合模式
		 */
		ac.setEncryptMessage(ConfigKit.getBoolean("encryptMessage", false));
		ac.setEncodingAesKey(ConfigKit.getStr("encodingAesKey",
				"setting it in config file"));
		return ac;
	}
	
	
	public void index(){
		List<Map> userInfos=new ArrayList<Map>();
		List<String> allOpenId = getAllOpenId();
		int total=allOpenId.size();
		UserConfig[] user_list=null;
		//开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
		int temp=100;//一次获取100
		if (total>temp) {
			int page=0;//当前页面
			int count=total/100+(total%100>0?1:0);//总共获取多少次
			int index=0;
			while (page<count) {
				index=(temp*(page+1))>total?total:(temp*(page+1));
				System.out.println("/////////"+page*temp+" "+index);
				user_list=new UserConfig[index-(page*temp)];
				for (int i = page*temp; i <index; i++) {
					UserConfig config=new UserConfig();
					config.setLang(UserConfig.LangType.zh_CN);
					config.setOpenid(allOpenId.get(i));
					user_list[i-(page*temp)]=config;
				}
				GetUserInfo getUserInfo = new GetUserInfo();
				getUserInfo.setUser_list(user_list);
				String jsonGetUserInfo = JsonKit.toJson(getUserInfo);
				System.out.println("jsonGetUserInfo："+jsonGetUserInfo);
//				ApiResult batchGetUserInfo = UserApi.batchGetUserInfo(jsonGetUserInfo);
				
				String jsonResult = HttpKit.post(batchGetUserInfo + AccessTokenApi.getAccessTokenStr(), jsonGetUserInfo);
				//将json转化为对象
				List<Map> userInfo = parseJsonToUserInfo(jsonResult);
				userInfos.addAll(userInfo);
				page++;
				try {
					Thread.sleep(1000*2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			///下载userInfos
			saveToExcel(userInfos);
		}else {
			user_list=new UserConfig[total];
			for (int i = 0; i < user_list.length; i++) {
				System.out.println(allOpenId.get(i));
				UserConfig config=new UserConfig();
				config.setLang(UserConfig.LangType.zh_CN);
				config.setOpenid(allOpenId.get(i));
				user_list[i]=config;
			}
			GetUserInfo getUserInfo = new GetUserInfo();
			getUserInfo.setUser_list(user_list);
			String jsonGetUserInfo = JsonKit.toJson(getUserInfo);
			
			
			ApiResult batchGetUserInfo = UserApi.batchGetUserInfo(jsonGetUserInfo);
			List<Map> userInfo = parseJsonToUserInfo(batchGetUserInfo.getJson());
			userInfos.addAll(userInfo);
			
			saveToExcel(userInfos);
		}
	}
	/**
	 * 获取多有的openid
	 * @return
	 */
	private List<String> getAllOpenId(){
		List<String> openIds = getOpenIds(null);
		return openIds;
	}
	
	private List<String> getOpenIds(String next_openid){
		List<String> openIdList=new ArrayList<String>();
		ApiResult apiResult=UserApi.getFollowers(next_openid);
		String json=apiResult.getJson();
		log.error("json:"+json);
		if (apiResult.isSucceed()) {
			JSONObject result = JSON.parseObject(json);
			next_openid = apiResult.getStr("next_openid");
			int count = apiResult.getInt("count");
			JSONObject openIdObject = result.getJSONObject("data");
			if (count>0) {
				JSONArray openids=openIdObject.getJSONArray("openid");
				for (int i = 0; i < openids.size(); i++) {
					openIdList.add(openids.getString(i));
				}
			}
			//下一页
			if (next_openid!=null&& !next_openid.equals("")) {
				List<String> list = getOpenIds(next_openid);
				openIdList.addAll(list);
			}
		}
		return openIdList;
	}
	
	
	public void save(){
		List<Map> userInfos = new ArrayList<Map>();
		for (int i = 0; i < 10; i++) {
			userInfos.add(UserInfo.objectToMap(new UserInfo("city", "country", "groupid", "headimgurl","language", "nickname", "openid", "province", "remark", "sex", "subscribe", "subscribe_time")));
		}

		saveToExcel(userInfos);
	}
	
	
	private List<Map> parseJsonToUserInfo(String jsonResult){
		List<Map> list=new ArrayList<Map>();
		SimpleDateFormat sfg=new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		JSONObject jo=JSONObject.parseObject(jsonResult);
		
		System.out.println("jo>"+jo.toString());
		
		JSONArray user_list=jo.getJSONArray("user_info_list");
		
		for (int i = 0; i < user_list.size(); i++) {
			JSONObject userObject=user_list.getJSONObject(i);
			String city=userObject.getString("city");
			String country=userObject.getString("country");
			String groupid=userObject.getString("groupid");
			String headimgurl=userObject.getString("headimgurl");
			String language=userObject.getString("language");
			String nickname=userObject.getString("nickname");
			String openid=userObject.getString("openid");
			String province=userObject.getString("province");
			String remark=userObject.getString("remark");
			String sex=userObject.getString("sex");
			//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
			
			if (sex!=null && !sex.equals("")) {
				int sexInt=Integer.parseInt(sex);
				if (sexInt==1) {
					 sex="男";
				}else if (sexInt==2) {
					 sex="女";
				}else {
					sex="未知";
				}
			}
			else {
				sex="未知";
			}
			String subscribe=userObject.getString("subscribe");
			String subscribe_time=userObject.getString("subscribe_time");
			
			if (subscribe_time!=null && !subscribe_time.equals("")) {
				subscribe_time=sfg.format(new Date(Long.parseLong(subscribe_time) * 1000L));
			}
			
			list.add(UserInfo.objectToMap(new UserInfo(city, country, groupid, headimgurl, language, nickname, openid, province, remark, sex, subscribe, subscribe_time)));
		}
		return list;
		
		
	}

	private void saveToExcel(List<Map> userInfos){

		String[] headers = {"用户的昵称", "用户的标识", "性别", "所在国家", "所在省份", "所在城市", "用户的语言", "用户头像",   "是否订阅",   "关注时间",        "所在的分组ID", "备注"};
		String[] columns = {"nickname",  "openid",    "sex",  "country", "province", "city",   "language",  "headimgurl", "subscribe", "subscribe_time", "groupid",     "remark"};
		PoiRender excel = PoiRender.me(userInfos);
		String fileName = "user's data.xls";
		render(excel.sheetName("用户详细信息").headers(headers).columns(columns).fileName(fileName));
	}
	

	public static void main(String[] args) {
		String[] ss=null;
		List<String> list=new ArrayList<String>();
		for (int i = 0; i < 23; i++) {
			list.add("str"+i);
		}
		
		int j=5;//一次获取多少条
		int total =23;
		if (total>j) {
			int page=0;//当前页面
			int count=total/j+(total%j>0?1:0);//总共获取多少次
			int index=0;
			while (page<count) {
				index=(j*(page+1))>total?total:(j*(page+1));
				System.out.println("/////////"+page*j+" "+index);
				ss=new String[(index-(page*j))];
				System.out.println("ss>"+ss.length);
				for (int i = page*j; i <index; i++) {
					System.out.println(i-(page*j));
				}
				page++;
			}
		}
		
		SimpleDateFormat sfg=new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		
		System.out.println(sfg.format(new Date(1439221146*1000L)));
	}
}

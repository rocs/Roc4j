package me.roczh.roc4j.demo.service;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;
import com.jfinal.plugin.sqlmgr.SqlKit;
import me.roczh.roc4j.demo.model.Blog;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by peng on 2016/4/13.
 */
public class BlogService {

    public static List<Blog> findAll_(){
        List<Blog> blogList = Blog.dao.find("select * from tbl_blog order by publish_time desc");
        for(Blog blog : blogList){
            blog.remove("content");
        }
        return blogList;
    }

    public static Page<Blog> findAll(int pageNumber, int  pageSize){
        return Blog.dao.paginateByCache("me.roczh.roc4j.demo.model.Blog", "blogAll", pageNumber, pageSize, "select *", "from tbl_blog order by publish_time desc");
    }

    public static List<Blog> findByKeywords_(final String keywords) {
        LinkedList<Object> paras = new LinkedList<>();
        return Blog.dao.findByCache("me.roczh.roc4j.demo.model.Blog", "blogSearch", SqlKit.get(Blog.class.getName()+"."+"findByKeywords", new HashMap<String, String>(1){{
            put("keywords", keywords);
        }}, paras), paras.toArray());
    }

    @Before(Tx.class)
    public boolean save(Blog blog) {
        blog.setPublishTime(new java.sql.Timestamp(System.currentTimeMillis()));
        return blog.save();
    }

    @Before({Tx.class, EvictInterceptor.class})
    @CacheName("me.roczh.roc4j.demo.model.Blog")
    public boolean saveOrUpdate(Blog blog){
        try {
            if(blog.getId() == null){
                blog.set("id","seq_blog_id.nextval");
                return save(blog);
            }else if(blog.getId() > 0L){
                return blog.update();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}

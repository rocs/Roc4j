package me.roczh.roc4j.demo.view.format;

import org.beetl.core.Format;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * beetl页面日期格式化，重写是为了处理Oracle的TIMESTAMP
 *
 * Created by peng on 2016/5/5.
 */
public class DateFormatFormat implements Format {
    @Override
    public Object format(Object data, String pattern) {
        if (data == null)
            return null;
        if (Date.class.isAssignableFrom(data.getClass())) {
            SimpleDateFormat sdf = null;
            if (pattern == null) {
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            } else {
                sdf = new SimpleDateFormat(pattern);
            }
            return sdf.format((Date) data);

        } /*else if (TIMESTAMP.class.isAssignableFrom(data.getClass())) { // 针对Oracle JDBC特殊处理
            SimpleDateFormat sdf = null;
            if (pattern == null) {
                sdf = new SimpleDateFormat();
            } else {
                sdf = new SimpleDateFormat(pattern);
            }
            TIMESTAMP ts = (TIMESTAMP) data;
            Date date = null;
            try {
                date = ts.timestampValue();
            } catch (SQLException e) {
                return "";
            }

            return sdf.format(date);

        } */else if (data.getClass() == Long.class) {
            Date date = new Date((Long) data);
            SimpleDateFormat sdf = null;
            if (pattern == null) {
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            } else {
                sdf = new SimpleDateFormat(pattern);
            }
            return sdf.format((Date) data);

        } else {
            throw new RuntimeException("Arg Error:Type should be Date:" + data.getClass());
        }
    }
}

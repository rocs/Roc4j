package me.roczh.roc4j.demo.mpweixin;

import com.jfinal.config.Routes;
import me.roczh.roc4j.demo.mpweixin.share.JssdkController;
import me.roczh.roc4j.demo.mpweixin.usercontroller.UserController;
import me.roczh.roc4j.demo.mpweixin.weboauth2.RedirectUriController;

/**
 * Created by peng on 2016/4/21.
 */
public class WeixinRoutes extends Routes {
    @Override
    public void config() {
        add("/", IndexController.class);
        add("/msg", WeixinMsgController.class);
        add("/api", WeixinApiController.class);
        add("/pay", WeixinPayController.class, "/wxpay");
        add("/oauth2", RedirectUriController.class);
        add("/jssdk", JssdkController.class, "mpjssdk");
        add("/user", UserController.class);
    }
}

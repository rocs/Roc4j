package me.roczh.roc4j.demo.qyweixin;

import com.jfinal.config.Routes;

/**
 * Created by peng on 2016/4/19.
 */
public class QyWeixinRoutes extends Routes {
    @Override
    public void config() {
        add("/", QyIndexController.class);	// 第三个参数为该Controller的视图存放路径
        add("/qymsg", QyWeixinMsgController.class);
        add("/qyapi", QyWeixinApiController.class);
        add("/qyoauth2", QyOAuthController.class);
        add("/qyjssdk", QyJssdkController.class);
    }
}

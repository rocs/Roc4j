package me.roczh.roc4j.demo.msg;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;
import com.jfinal.websocket.Message;

/**
 * Created by Rocs on 2016/3/19.
 */
public class TestMessage extends Message {

    public static final int MSG_CODE = 1010;

    @Override
    public int getMsgcode() {
        return MSG_CODE;
    }

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static TestMessage create(String content){
        TestMessage message = new TestMessage();
        message.setContent(content);
        message.setCreatetime(System.currentTimeMillis());
        return message;
    }

    @Override
    public boolean valid() {
        return !StrKit.isBlank(getContent());
    }

    @Override
    public void onReceived() {
        LogKit.info("收到一条测试消息内容：" + getContent());
    }
}

package me.roczh.roc4j.demo.qyweixin;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.route.AutoBindRoutes;
import com.jfinal.plugin.config.ConfigKit;
import com.jfinal.plugin.config.ConfigPlugin;
import com.jfinal.qy.weixin.sdk.api.ApiConfigKit;

public class ApplicationConfig extends JFinalConfig{

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		ConfigPlugin.setSuffix("conf");
		new ConfigPlugin(".*.conf").reload(true).start();
		me.setDevMode(ConfigKit.getBoolean("devMode", true));
		// ApiConfigKit 设为开发模式可以在开发阶段输出请求交互的 xml 与 json 数据
		ApiConfigKit.setDevMode(me.getDevMode());
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add(new QyWeixinRoutes());
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 80, "/", 5);//启动配置项
	}

}

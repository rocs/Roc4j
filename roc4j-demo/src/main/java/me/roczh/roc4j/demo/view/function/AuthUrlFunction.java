package me.roczh.roc4j.demo.view.function;

import com.jfinal.kit.LogKit;
import me.roczh.roc4j.demo.core.AuthInterceptor;
import org.beetl.core.Context;
import org.beetl.core.Function;

/**
 * 页面按钮权限验证函数
 *
 * Created by peng on 2016/5/5.
 */
public class AuthUrlFunction implements Function {
    @Override
    public Object call(Object[] paras, Context ctx) {
        if(paras.length != 1 || null == paras[0]){
            LogKit.error("权限标签验证，参数不正确");
            return false;
        }
        String url  = (String) paras[0];
        String userIds = ctx.getGlobal("_current_account_ids") == null ? "" : ctx.getGlobal("_current_account_ids").toString();
        return  AuthInterceptor.hasPrivilegeUrl(userIds, url);
    }
}

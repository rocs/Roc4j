package me.roczh.roc4j.demo.view;

import me.roczh.roc4j.demo.model.Blog;
import me.roczh.roc4j.demo.view.format.DateFormatFormat;
import me.roczh.roc4j.demo.view.function.AuthUrlFunction;
import me.roczh.roc4j.demo.view.tag.AssetsTag;
import org.beetl.core.Context;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.VirtualClassAttribute;
import org.beetl.ext.jfinal.BeetlRenderFactory;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 模板工具类
 *
 * Created by peng on 2016/5/5.
 */
public final class TemplateKit {

    /**
     * 模板扩展
     */
    public static GroupTemplate regiseter(){
        GroupTemplate mainGT = BeetlRenderFactory.groupTemplate;

        mainGT.setSharedVars(new HashMap<String, Object>() {
            {
                put("version", "1.0.0");
            }
        });
        mainGT.registerVirtualAttributeClass(Blog.class, new VirtualClassAttribute() {
            @Override
            public Object eval(Object o, String attributeName, Context ctx) {
                Blog blog = (Blog)o;
                if(attributeName.equals("publishTime")){
                    return blog.getPublishTime();
                }
                return null;
            }
        });
        mainGT.registerFunction("authUrl", new AuthUrlFunction());
        mainGT.registerTag("assets", AssetsTag.class);
        mainGT.registerFormat("dateFormat", new DateFormatFormat());

        return mainGT;
    }

    /**
     * 生成静态html
     *
     * @param templatePath 模板路径
     * @param paramMap 参数
     * @param htmlPath html文件保存路径
     */
    public static void makeHtml(String templatePath, Map<String, Object> paramMap, String htmlPath) {
        PrintWriter pw = null;
        try {
            GroupTemplate groupTemplate = regiseter();
            Template template = groupTemplate.getTemplate(templatePath);
            //添加数据到上下文中
            Set<String> keys = paramMap.keySet();
            for (String key : keys) {
                template.binding(key, paramMap.get(key));
            }
            pw = new PrintWriter(htmlPath);
            template.renderTo(pw);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(pw != null ){
                pw.close();
            }
        }
    }
}

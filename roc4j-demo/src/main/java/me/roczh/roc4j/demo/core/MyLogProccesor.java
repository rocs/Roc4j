package me.roczh.roc4j.demo.core;

import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.syslog.LogProcessor;
import com.jfinal.ext.interceptor.syslog.SysLog;
import com.jfinal.kit.LogKit;

import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by peng on 2016/4/15.
 */
public class MyLogProccesor implements LogProcessor {

    private static boolean threadRun = true; // 线程是否运行
    private static Queue<SysLog> queue = new ConcurrentLinkedQueue<SysLog>(); //	此队列按照 FIFO（先进先出）原则对元素进行排序

    @Override
    public void process(SysLog sysLog) {
        queue.offer(sysLog);
    }

    @Override
    public String getUsername(Controller c) {
        //return c.getSessionAttr("username");
        return "<未知>";
    }

    @Override
    public String formatMessage(String title, Map<String, String> message) {
        String result = title;
        if (message.isEmpty()) {
            return result;
        }
        result += ", ";
        Set<Map.Entry<String, String>> entrySet = message.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            String key = entry.getKey();
            String value = entry.getValue();
            result += (key + ":" + value + " ");
        }
        return result;
    }


    public static void setThreadRun(boolean threadRun) {
        MyLogProccesor.threadRun = threadRun;
    }
    /**
     * 启动入库线程
     *
     * @param num   启动线程数量
     */
    public static void startSaveDBThread(int num) {
        try {
            for (int i = 0; i < num; i++) {
                Thread insertDbThread = new Thread(new Runnable() {
                    public void run() {
                        while (threadRun) {
                            try {
                                SysLog sysLog = queue.poll();
                                if(null == sysLog){
                                    Thread.sleep(200);
                                } else {
                                    sysLog.saveToDB();  //入库
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
                insertDbThread.setName("Thread-SysLog-insertDB-" + (i + 1));
                insertDbThread.start();
            }
        } catch (Exception e) {
            throw new RuntimeException("MyLogProccesor new Thread Exception");
        }
    }

}

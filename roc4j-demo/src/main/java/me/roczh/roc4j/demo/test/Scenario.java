package me.roczh.roc4j.demo.test;

import java.io.File;

/**
 * 业务场景下生产代码
 *
 * @author Peng
 */
public class Scenario {

    // 冒泡排序
    public int[] sort(int[] array) throws Exception {
        if (array == null || array.length == 0) {
            throw new Exception("所输入的参数不合法！");
        }
        for (int i = 0; i < array.length - 1; i++) {
            boolean swap = false;
            int temp;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swap = true;
                }
            }
            if (!swap) {
                break;
            }
        }
        return array;
    }

    // 统计文件数量
    public int getFilesCount(File file) {
        int number = 0;
        if (!file.exists() || 0 == file.listFiles().length ) {
            return 0;
        }
        if(file.isFile()){
            return 1;
        }
        File[] files = file.listFiles();
        for(File f : files){
            if(f.isFile()){
                ++number;
            }else{
                number += getFilesCount(f);
            }
        }
        return number;
    }

}

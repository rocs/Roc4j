package me.roczh.roc4j.demo.core;

import com.jfinal.plugin.config.ConfigKit;

/**
 * 系统常量
 *
 * Created by peng on 2016/5/5.
 */
public final class Consts {

    /**
     * 系统版本号
     */
    public static final String CONFIG_VERSION_SYSTEM = ConfigKit.getStr("config.version.system","V 1.0.0");

}

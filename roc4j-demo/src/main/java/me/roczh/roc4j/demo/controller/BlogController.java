/**
 * 
 */
package me.roczh.roc4j.demo.controller;

import com.jfinal.aop.Before;
import com.jfinal.aop.Enhancer;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.ext.render.csv.CsvRender;
import com.jfinal.ext.render.excel.PoiRender;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;
import com.jfinal.render.JsonRender;
import com.jfinal.upload.UploadFile;
import com.jfinal.validate.Validator;
import me.roczh.roc4j.demo.model.Blog;
import me.roczh.roc4j.demo.service.BlogService;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * 
 * @author Rocs
 *
 */
public class BlogController extends Controller {

	BlogService blogService = Enhancer.enhance(BlogService.class);

	@ActionKey("/test")
	public void test(){
		LogKit.info("redirect {0} >>", "test page");
		redirect("/assets/test.html");
	}

	public void index(){
		list();
		render("list.html");
	}
	
	public void list(){
		Integer pageNumber = getParaToInt("page");
		if(pageNumber == null){
			pageNumber = 1;
		}
		Integer pageSize = getParaToInt("size");
		if(pageSize == null){
			pageSize = 10;
		}
		setAttr("blogs", blogService.findAll(pageNumber, pageSize));
	}

	public void search(){
		String keywords = getPara("keywords");
		setAttr("blogs", blogService.findByKeywords_(keywords));
		keepPara();
		render("list.html");
	}
	
	public void form(){
		Long blogId = getParaToLong("id");
		if(blogId != null && blogId > 0){
			Blog blog = Blog.dao.findById(blogId);
			setAttr("blog", blog);
		}
	}

	/*@Before(saveValidator.class)*/
	public void save(){
		UploadFile file = getFile("blog.attachement");
		if(file == null || file.getFile() == null){
			setAttr("errors", new String[]{"请选择上传一张图片"});
			keepModel(Blog.class);
			render("form.html");
			return;  //不再执行下去，即redirect("")不会生效
		}
		System.out.println("upload to " + file.getFile().getPath());
		Blog blog = getModel(Blog.class);
		blogService.saveOrUpdate(blog);
		redirect("/blog/");
	}
	public static class saveValidator extends Validator{
		@Override
		protected void validate(Controller c) {
			validateRequiredString("blog.tITLE","titleMsg","请输入标题");
			validateRequiredString("blog.cONTENT","contentMsg","请输入内容");
		}
		@Override
		protected void handleError(Controller c) {
			c.keepModel(Blog.class);
			c.render("form.html");
		}
	}

	@Before(EvictInterceptor.class)
	@CacheName("me.roczh.roc4j.demo.model.Blog")
	public void del(){
		Long blogId = getParaToLong("id");
		if(blogId != null && blogId > 0 && Blog.dao.deleteById(blogId)){
			redirect("");
		}else{
			renderText("删除失败!");
		}
	}
	
	public void download(){
		renderFile("test.txt");
	}
	
	public void upload(){
		
	}
	
	public void ajaxUpload(){
		UploadFile file = getFile("blog.attachement");
//		renderJson("imgUrl","/upload/" + file.getFileName());
		//这个虽然是forIE 但是其他浏览器也有效 ie也好用
		render(new JsonRender("imgUrl","/upload/" + file.getFileName()).forIE());
	}
	
	public void list4XML(){
		setAttr("blogs", blogService.findAll_());
		renderXml("/renderxml/blogs.bxml");
	}

	public void exportExcel(){
		String[] headers = {"标题", "内容"};
		String[] columns = {"title", "content"};
		PoiRender excel = PoiRender.me(BlogService.findAll_());
		String fileName = "blog's data.xls";
		render(excel.sheetName("博文数据").headers(headers).columns(columns).fileName(fileName));
	}

	public void exportCSV(){
		List<String> headers = Arrays.asList(new String[]{"标题", "内容"});
		List<String> columns = Arrays.asList(new String[]{"title", "content"});
		CsvRender csv = CsvRender.me(headers, BlogService.findAll_());
		String fileName = "blog's data.csv";
		render(csv.clomuns(columns).fileName(fileName));
	}

	public void ajax(){
		list();
	}

	public void ajaxPage(){
		list();
		render("ajax.html#blogTable");
	}
}

package me.roczh.roc4j.demo.core;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.config.ConfigKit;
import com.jfinal.plugin.ehcache.CacheKit;
import me.roczh.roc4j.demo.model.Account;
import me.roczh.roc4j.demo.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 权限认证
 *
 * Created by peng on 2016/5/3.
 */
public class AuthInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation inv) {
        Controller contr = inv.getController();
        HttpServletRequest req = contr.getRequest();
        HttpServletResponse resp = contr.getResponse();
        String uri = inv.getActionKey(); // 默认就是ActionKey
        boolean validateUserAgentOrNot = true; // 是否验证userAgent，默认是
        if (uri.equals("/ueditor")) { // 针对ueditor特殊处理
            validateUserAgentOrNot = false;
        }
        Account account = getCurrentAccount(req, resp, validateUserAgentOrNot);// 当前登录用户
        if (null != account) {
            contr.setAttr("_current_account", account);
            contr.setAttr("_current_account_ids", account.obtainPKValue());
        }else{
            contr.redirect("/login");
            return;
        }
        if (!hasPrivilegeUrl(account.obtainPKValue(), uri)) {// 权限验证
            String isAjax = contr.getRequest().getHeader("X-Requested-With");
            if(isAjax != null && isAjax.equalsIgnoreCase("XMLHttpRequest")){
                contr.render("/common/msgAjax.html"); // Ajax页面
            }else{
                contr.render("/common/msg.html"); // 完整html页面
            }
            return;
        }
        inv.invoke();
    }


    /**
     * 获取当前登录帐号
     * @param request
     * @param response
     * @param validateUserAgentOrNot 是否验证 User-Agent
     * @return
     */
    public static Account getCurrentAccount(HttpServletRequest request, HttpServletResponse response, boolean validateUserAgentOrNot) {
        // 加密串存储位置，默认先取header
        String store = "header";
        String loginCookie = request.getHeader("authmark");
        if(loginCookie == null || loginCookie.isEmpty()){
            // 如果为空，再取cookie
            store = "cookie";
            loginCookie = getCookieValueByName(request, "authmark");
        }
        // 处理加密串的解析和验证
        if (null != loginCookie && !loginCookie.isEmpty()) {
            // 1.解密认证数据
            String data = StringUtils.decryptString(loginCookie);
            if(null == data || data.isEmpty()){
                addCookie(response, "", "/", true, "authmark", null, 0);
                return null;
            }
            String[] datas = data.split(".#.");	//arr[0]：时间戳，arr[1]：USERID，arr[2]：USER_IP， arr[3]：USER_AGENT
            // 2. 分解认证数据
            long loginDateTimes;
            String userIds = null;
            String ips = null;
            String userAgent = null;
            boolean autoLogin = false;
            try {
                loginDateTimes = Long.parseLong(datas[0]); // 时间戳
                userIds = datas[1]; // 用户id
                ips = datas[2]; // ip地址
                userAgent = datas[3]; // USER_AGENT
                autoLogin = Boolean.valueOf(datas[4]); // 是否自动登录
            } catch (Exception e) {
                if(store.equals("cookie")){
                    addCookie(response, "", "/", true, "authmark", null, 0);
                }
                return null;
            }
            // 3.用户当前数据
            String newIp = getIpAddr(request);
            String newUserAgent = request.getHeader("User-Agent");
            Date start = new Date();
            start.setTime(loginDateTimes); // 用户自动登录开始时间
            int day = getDateDaySpace(start, new Date()); // 已经登录多少天
            int maxAge = ConfigKit.getInt("config.cookie.maxAge");
            // 4. 验证数据有效性
            if (ips.equals(newIp) && (validateUserAgentOrNot ? userAgent.equals(newUserAgent) : true) && day <= maxAge) {
                // 如果不记住密码，单次登陆有效时间验证
                if(!autoLogin){
                    int minute = getDateMinuteSpace(start, new Date());
                    int session = ConfigKit.getInt("config.session.timeout");
                    if(minute > session){
                        return null;
                    }else{
                        // 重新生成认证cookie，目的是更新时间戳
                        long date = new Date().getTime();
                        StringBuilder token = new StringBuilder(); // 时间戳.#.USERID.#.USER_IP.#.USER_AGENT.#.autoLogin
                        token.append(date).append(".#.").append(userIds).append(".#.").append(ips).append(".#.").append(userAgent).append(".#.").append(autoLogin);
                        String authmark = StringUtils.encryptString(token.toString());
                        // 添加到Cookie
                        if(store.equals("cookie")){
                            int maxAgeTemp = -1; // 设置cookie有效时间
                            addCookie(response,  "", "/", true, "authmark", authmark, maxAgeTemp);
                        }else if(store.equals("header")){
                            response.setHeader("authmark", authmark);
                        }
                    }
                }
                // 返回用户数据
                Object userObj = CacheKit.get("system", userIds);
                if (null != userObj) {
                    Account user = (Account) userObj;
                    return user;
                }
            }
        }
        return null;
    }


    /**
     * 判断用户是否拥有某个url的操作权限
     *
     * @param accountIds
     * @param url
     * @return
     */
    public static boolean hasPrivilegeUrl(String accountIds, String url) {
       //TODO: 判断用户是否拥有某个url的操作权限
        return true;
    }


    /**
     * 获取cookie的值
     */
    public static String getCookieValueByName(HttpServletRequest request, String name) {
        Map<String, Cookie> cookieMap = new HashMap<String, Cookie>();
        // 从request范围中得到cookie数组 然后遍历放入map集合中
        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (int i = 0; i < cookies.length; i++) {
                cookieMap.put(cookies[i].getName(), cookies[i]);
            }
        }
        // 判断cookie集合中是否有我们像要的cookie对象 如果有返回它的值
        if (cookieMap.containsKey(name)) {
            Cookie cookie = (Cookie) cookieMap.get(name);
            return cookie.getValue();
        } else {
            return null;
        }
    }

    /**
     * 设置cookie的值
     *
     * @param response
     * @param domain		设置cookie所在域
     * @param path			设置cookie所在路径
     * @param isReadOnly	是否只读
     * @param name			cookie的名称
     * @param value			cookie的值
     * @param maxAge		cookie存放的时间(以秒为单位,假如存放三天,即3*24*60*60; 如果值为0,cookie将随浏览器关闭而清除)
     */
    public static void addCookie(HttpServletResponse response,
                                 String domain, String path, boolean isReadOnly,
                                 String name, String value, int maxAge) {

        Cookie cookie = new Cookie(name, value);
        // 所在域：比如a1.4bu4.com 和 a2.4bu4.com 共享cookie
        if(null != domain && !domain.isEmpty()){
            cookie.setDomain(domain);
        }
        // 设置cookie所在路径
        cookie.setPath("/");
        if(null != path && !path.isEmpty()){
            cookie.setPath(path);
        }
        // 是否只读
        try {
            cookie.setHttpOnly(isReadOnly);
        } catch (Exception e) {
            LogKit.error("servlet容器版本太低，servlet3.0以前不支持设置cookie只读" + e.getMessage());
        }
        // 设置cookie的过期时间
        if (maxAge > 0){
            cookie.setMaxAge(maxAge);
        }
        // 添加cookie
        response.addCookie(cookie);
    }

    /**
     * 获取客户端IP地址
     * @param request
     * @return
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    private static int getDateDaySpace(Date start, Date end) {
        int day = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 24 * 1000));
        return day;
    }

    public static int getDateMinuteSpace(Date start, Date end) {
        int hour = (int) ((end.getTime() - start.getTime()) / (60 * 1000));
        return hour;
    }
}

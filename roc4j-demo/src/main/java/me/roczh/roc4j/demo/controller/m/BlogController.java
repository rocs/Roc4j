/**
 * 
 */
package me.roczh.roc4j.demo.controller.m;

import java.sql.Timestamp;
import java.util.*;

import com.jfinal.aop.Before;
import com.jfinal.aop.Enhancer;
import com.jfinal.core.Controller;

import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.kit.SessionKit;
import com.jfinal.i18n.I18n;
import com.jfinal.i18n.Res;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;
import com.jfinal.plugin.ehcache.IDataLoader;
import com.jfinal.render.JsonRender;
import me.roczh.roc4j.demo.model.Blog;
import me.roczh.roc4j.demo.service.BlogService;

/**
 * @author Rocs
 *
 */
public class BlogController extends Controller {

	BlogService blogService = Enhancer.enhance(BlogService.class);

	public void index(){
		test3();
	}
	
	public void test1(){
		setAttr("code", 4);
		setAttr("msg","Not Found the key!");
		setAttr("data","");
		renderJson(new String[]{"code", "msg"});
	}
	
	public void test2(){
		List<Blog> blogList = CacheKit.get("me.roczh.roc4j.demo.model.Blog", "blogAll", new IDataLoader() {
			@Override
			public Object load() {
				return blogService.findAll_();
			}
		});
		renderJson(blogList);
	}
	
	public void test3(){
		Map<String,Object> result = new HashMap<String, Object>();
		result.put("code", 0);
		result.put("data", blogService.findAll_());
		renderJson(JsonKit.toJson(result, Blog.class, new String[]{"publishTime"}));
	}
	
	public void test4(){
		Map<String,Object> result = new HashMap<String, Object>();
		result.put("code", 4);
		result.put("msg", "Not Found the key!");
		render(new JsonRender(result).forIE());
	}

	public void list(){
		//List<Record> blogs = Db.find("select * from tbl_blog order by publish_time desc");
		render(new JsonRender(Blog.dao.findAll()).forIE());
	}

	@Before({Tx.class, EvictInterceptor.class})
	@CacheName("me.roczh.roc4j.demo.model.Blog")
	public void randomSave(){
		Integer total = getParaToInt("total");
		if(total != null && total > 1){
			List<Record> blogList = new ArrayList<>(total);
			for(int i = 0; i < total; i++){
				Record blog = new Record();
				long currentTimeMillis = System.currentTimeMillis();
				blog.set("title", "标题" + currentTimeMillis).set("content", "内容" + currentTimeMillis).set("publish_time", new Timestamp(currentTimeMillis));
				Db.save("tbl_blog", blog);
				blogList.add(blog);
			}
			render(new JsonRender(blogList).forIE());
		}else{
			Record blog = new Record();
			long currentTimeMillis = System.currentTimeMillis();
			blog.set("title", "标题" + currentTimeMillis).set("content", "内容" + currentTimeMillis).set("publish_time", new Timestamp(currentTimeMillis));
			Db.save("tbl_blog", blog);
			render(new JsonRender(blog).forIE());
		}
	}
	
	public void del(){
		Long blogId = getParaToLong("id");
		if(blogId != null && blogId > 0 && Db.deleteById("tbl_blog", blogId)){
			renderText("删除" + blogId);
		}else{
			renderText("删除失败!");
		}
	}

	public void rnull(){
		renderNull();
	}

	public void rreturn(){
/*		renderText("1");
		renderText("2");*/
		return;
	}

	public void ri18n(){
		Res res = I18n.use("zh_CN");
		String msg = res.get("msg");
		String msgFormat = res.format("msg", "张三", new Date());
		renderText(msgFormat);
	}

	public void rcapcha(){
		renderCaptcha();
	}

	public void rerror1(){
		throw new IllegalArgumentException();
	}

	public void rerror2(){
		throw new RuntimeException();
	}

	public void session(){

		//获取一个新的session
		Session session = SessionKit.getNewSession();
		session.setAttribute("hello", "world");
		//添加session
		SessionKit.addSession(session);
		//删除session
		//SessionKit.removeSession(session.getId());

		redirect("/m/blog/rsession?si=" + session.getId());
	}

	public void rsession(){
		String sessionId = null;
		if(!StrKit.isBlank(sessionId = getPara("si"))){
			Session session = SessionKit.getSession(sessionId);
			if(session != null){
				Object val = session.getAttribute("hello");
				if( val != null){
					renderText(val.toString());
				}else{
					renderText("null-k");
				}
			}else{
				renderText("null-s");
			}
		}else{
			renderText("null-p");
		}
	}
}

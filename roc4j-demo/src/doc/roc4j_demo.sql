
/* Drop Tables */

DROP TABLE IF EXISTS tbl_blog;
DROP TABLE IF EXISTS tbl_account;
DROP TABLE IF EXISTS tbl_syslog;
DROP TABLE IF EXISTS tbl_session;



/* Drop Sequences */

DROP SEQUENCE seq_blog_id;
DROP SEQUENCE seq_account_id;




/* Create Sequences */

CREATE SEQUENCE IF NOT EXISTS seq_blog_id;
CREATE SEQUENCE IF NOT EXISTS seq_account_id;


/* Create Tables */

-- 博文
CREATE TABLE tbl_blog
(
	-- ID
	id bigint DEFAULT nextval('seq_blog_id') NOT NULL,
	-- 文章标题
	title varchar(200),
	-- 正文内容
	content clob,
	-- 发布时间
	publish_time timestamp,
	-- 锁位
	version bigint,
	create_time timestamp,
	update_time timestamp,
	disable boolean,
	PRIMARY KEY (id)
);

-- 账号
CREATE TABLE tbl_account
(
	-- ID
	id bigint DEFAULT nextval('seq_account_id') NOT NULL,
	-- 登陆名
	login_name varchar(64),
	-- 登陆密码
	password varchar(64),
	-- 锁位
	version bigint,
	create_time timestamp,
	update_time timestamp,
	disable boolean,
	PRIMARY KEY (id)
);

-- 系统日志
CREATE TABLE tbl_syslog
(
	-- ID
	id varchar(64) NOT NULL,
	-- 登陆用户
	user varchar(64),
	-- 请求IP
	ip varchar(64),
	-- 消息内容
	message clob,
	-- 锁位
	version bigint,
	create_time timestamp,
	update_time timestamp,
	PRIMARY KEY (id)
);


-- db session 存储
CREATE TABLE tbl_session
(
	si varchar(64) NOT NULL,
	so blob NOT NULL,
	lat bigint NOT NULL,
	PRIMARY KEY (si)
);


/**
 * 
 */
package perf.framework;

import com.clarkware.junitperf.*;
import junit.framework.Test;

/**
 * Test Method@TestCase ->  LoadTest
 * @author Peng
 */
public class TestPerfUtil {

    /**
     *
     * @param users
     * @param iters
     * @param delay
     * @param variation
     * @param clazz   TestCase Class
     * @param method
     * @return
     */
	public static Test suite(int users, int iters, long delay, double variation, Class<? extends Object> clazz, String method) {
		Test testCase = new TestMethodFactory(clazz, method);
        Timer timer = null;
        if(delay > 0 && variation == 0){
             timer = new ConstantTimer(delay);
        }
        if(variation != 0){
            timer = new RandomTimer(delay, variation);
        }
        Test loadTest = null;
        if(timer != null){
            loadTest = new LoadTest(testCase, users, iters, timer);
        }else{
            loadTest = new LoadTest(testCase, users, iters);
        }
		return loadTest;
	}
}

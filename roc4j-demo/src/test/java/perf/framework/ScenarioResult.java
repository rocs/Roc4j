/**
 * 
 */
package perf.framework;

import java.util.ArrayList;
import java.util.List;

/**
 * 对测试场景(含统计结果)的封装
 * 
 * @author Peng
 *
 */
public class ScenarioResult {

	/**
	 * NO.编号
	 */
	private int number;
	/**
	 * scenario场景描述
	 */
	private String description;
	/**
	 * 一[NO. scenario]对多[users iters]的测试结果[ max min avg]
	 */
	private List<List<Long>> results = new ArrayList<List<Long>>();
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<List<Long>> getResults() {
		return results;
	}
	public void setResults(List<List<Long>> results) {
		this.results = results;
	}
	
}

/**
 * 
 */
package perf.framework;

import junit.textui.TestRunner;
import perf.entry.ScenarioPerfHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Peng
 * 
 */
public class GenericUtil {

	/**
	 * 求平均时间
	 * 
	 * @param timeRecords
	 * @return
	 */
	private static long getAverageTime(Collection<Long> timeRecords) {
		int size = timeRecords.size();
		if (0 == size) {
			return 0L;
		}
		long totalTime = 0L, averageTime;
		for (Long time : timeRecords) {
			totalTime += time;
		}
		averageTime = totalTime / size;
		return averageTime;
	}

	/**
	 * 
	 * @param users
	 *            并发用户数
	 * @param iters
	 *            用户执行次数
	 * @param timeRecords
	 *            所记录的执行时间
	 * @return
	 */
	public static List<Long> getOneRowData(int users, int iters, List<Long> timeRecords) {
		List<Long> result = new ArrayList<Long>();
		long maxTime = Collections.max(timeRecords);
		long minTime = Collections.min(timeRecords);
		long avgTime = getAverageTime(timeRecords);
		result.add((long) users);
		result.add((long) iters);
		result.add(maxTime);
		result.add(minTime);
		result.add(avgTime);
		return result;
	}

	/**
	 * 
	 * @param bean
	 *            测试场景信息
	 * @param users
	 *            并发用户数
	 * @param iters
	 *            用户执行次数
	 * @param delay
	 *            用户执行间隔基准
     * @param variation
     *            用户执行间隔浮动因子
	 * @param clazz
	 *            所针对的单元测试类
	 * @param method
	 *            所针对的单元测试方法
	 */
	public static void setScenarioAttributes(ScenarioResult bean, int users, int iters, long delay, double variation, Class<? extends Object> clazz, String method) {
		//真正执行JUnitPerf测试
		TestRunner.run(TestPerfUtil.suite(users, iters, delay, variation, clazz, method));
		List<Long> oneRowData = getOneRowData(users, iters, ScenarioPerfHelper.getTimeRecords());
		bean.getResults().add(oneRowData);
		ScenarioPerfHelper.cleanTimeRecords();
	}

	public static void equipOneScenaric(ScenarioResult bean, Class<? extends Object> clazz, String method) {
		setScenarioAttributes(bean, 10, 10, 0, 0, clazz, method);
	}
}

/**
 * 
 */
package perf.framework;

import perf.entry.ScenarioPerfHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于装配ScenarioResult对象
 * 
 * @author Peng
 *
 */
public class ScenarioContext {

	private static List<ScenarioResult> resultRecords = new ArrayList<ScenarioResult>();

	public static List<ScenarioResult> getResultRecords() {
		return resultRecords;
	}
	
	public static void equip(int number, String description, String method){
		ScenarioResult scenarioResult = new ScenarioResult();
		scenarioResult.setNumber(number);
		scenarioResult.setDescription(description);
		GenericUtil.equipOneScenaric(scenarioResult, ScenarioPerfHelper.class, method);
		//在内存中存放测试结果
		resultRecords.add(scenarioResult);
	}
	
}

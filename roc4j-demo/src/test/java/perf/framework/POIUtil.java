/**
 * 
 */
package perf.framework;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Peng
 * 
 */
public class POIUtil {

	private static final String TITLE = "Performance Test Results";
    private static File file = null;
	private static Workbook wb =  null;
    static{
        String targetDir = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        targetDir = targetDir.substring(1, targetDir.length() - 1);
        targetDir = targetDir.substring(0, targetDir.lastIndexOf("/"));
        String tempDir = targetDir;
        targetDir += "/surefire-reports";
        if(!new File(targetDir).exists()){
            targetDir = tempDir.substring(0, tempDir.lastIndexOf("/"));
        }
        file = new File(targetDir + "/" + TITLE + ".xls");
        if(file.exists()){
            try {
                wb = new HSSFWorkbook(new FileInputStream(file));
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }else{
            wb = new HSSFWorkbook();
        }
    }
	private static Sheet sheet = wb.createSheet(new Date().toLocaleString().replaceAll(":","").trim());
	private static CreationHelper createHelper = wb.getCreationHelper();

	private static void generateTitle() {
		Row row = sheet.createRow(0);
		row.setHeight((short) 1200); // 标题行固定高度
		sheet.setColumnWidth(7, 6000); //固定宽度

		Font font = wb.createFont(); // 标题字体
		font.setFontName("Verdana");
		font.setBoldweight((short) 100);
		font.setFontHeight((short) 300);
		font.setColor(IndexedColors.RED.getIndex());
		CellStyle style = wb.createCellStyle();
		style.setFont(font); // 设置标题字体
		style.setAlignment(CellStyle.ALIGN_CENTER); // 设置水平对齐
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // 设置垂直对齐
		style.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());	//设置前景色
		style.setFillPattern(CellStyle.SOLID_FOREGROUND); // 设置填充模式

		Cell cell = row.createCell(0);
		cell.setCellValue(createHelper.createRichTextString(TITLE));
		cell.setCellStyle(style);

		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6)); //合并标题单元格
		
		cell = row.createCell(7);
		Calendar cal = Calendar.getInstance();
		String dateStr = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH);
		RichTextString text = createHelper.createRichTextString("Version:V0.1\n" + "Date:" + dateStr + "\nAuthor:Roc4j\nUnits:millisecond");
		cell.setCellValue(text);
		font = wb.createFont(); 
		font.setFontName("Verdana");
		font.setColor(IndexedColors.BLUE.getIndex());
		style = wb.createCellStyle();
		style.setFont(font); 
		style.setAlignment(CellStyle.ALIGN_LEFT); 
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER); 
		style.setWrapText(true); //支持自动换行
		style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());	//设置前景色
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);	
		cell.setCellStyle(style);
		
	}
	
	private static void generateHeader() {
		Row row = sheet.createRow(1);
		
		Font font = wb.createFont(); 
		font.setFontName("Verdana");
		font.setBoldweight((short) 1000);
		CellStyle style = wb.createCellStyle();
		style.setFont(font); 
		style.setAlignment(CellStyle.ALIGN_CENTER); 
		style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());	
		style.setFillPattern(CellStyle.SOLID_FOREGROUND); 
	    style.setBorderBottom(CellStyle.BORDER_THIN);  //蓝色底线
	    style.setBottomBorderColor(IndexedColors.BLUE.getIndex());
		
	    Cell[] cells = new Cell[7];
	    for(int i = 0 ; i < cells.length; i++){
	    	cells[i] = row.createCell(i);
	    	cells[i].setCellStyle(style);
	    }
	    cells[0].setCellValue(createHelper.createRichTextString("NO."));
	    cells[1].setCellValue(createHelper.createRichTextString("scenario"));
	    cells[2].setCellValue(createHelper.createRichTextString("users"));
	    cells[3].setCellValue(createHelper.createRichTextString("iters"));
	    cells[4].setCellValue(createHelper.createRichTextString("max"));
	    cells[5].setCellValue(createHelper.createRichTextString("min"));
	    cells[6].setCellValue(createHelper.createRichTextString("avg"));
	    
	}
	
	/**
	 * 
	 * @param datas
	 */
	public static void generateReport(List<ScenarioResult> datas){
		try {
			generateTitle();
			generateHeader();
			
			Font font = wb.createFont(); 
			font.setFontName("Verdana");
			CellStyle style = wb.createCellStyle();
			style.setFont(font); 
			style.setAlignment(CellStyle.ALIGN_CENTER); 
			style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    style.setBorderBottom(CellStyle.BORDER_THIN);  
		    style.setBottomBorderColor(IndexedColors.BLUE.getIndex());
		    CellStyle style2 = style;
		    style2.setAlignment(CellStyle.ALIGN_LEFT);
		    style2.setWrapText(true);
		    CellStyle style3 = wb.createCellStyle();
			style3.setAlignment(CellStyle.ALIGN_CENTER); 
			style3.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			style3.setFont(font); 
			
			int size = datas.size();
			int begLine = 2;
			for(ScenarioResult scenarioResult : datas){
				int count = scenarioResult.getResults().size();
				Row[] rows = new Row[count];
				Cell[] cellsNo = new Cell[count];
				Cell[] cellsDes = new Cell[count];
				for(int j = 0; j < count; j++){
					rows[j] = sheet.createRow(begLine + j);
					cellsNo[j] = rows[j].createCell(0);
					cellsDes[j] = rows[j].createCell(1);
				}
				cellsNo[0].setCellStyle(style);
				cellsNo[count-1].setCellStyle(style);
				cellsDes[0].setCellStyle(style);
				cellsDes[count-1].setCellStyle(style);
				sheet.addMergedRegion(new CellRangeAddress(begLine, begLine + count -1, 0 , 0)); 
				sheet.addMergedRegion(new CellRangeAddress(begLine, begLine + count -1, 1, 1)); 
			    
				cellsNo[0].setCellValue(scenarioResult.getNumber());
				cellsDes[0].setCellValue(createHelper.createRichTextString(scenarioResult.getDescription()));
				Cell[][] cellData = new Cell[count][5];
				for(int m = 0; m < count ; m++){
					for(int n = 0; n < 5; n++){
						cellData[m][n] = rows[m].createCell(2 + n);
						cellData[m][n].setCellStyle(style3);
						cellData[m][n].setCellValue(scenarioResult.getResults().get(m).get(n));
						if(count - 1 == m){
							cellData[m][n].getCellStyle().setBottomBorderColor(IndexedColors.BLUE.getIndex());
							cellData[m][n].getCellStyle().setBorderBottom(CellStyle.BORDER_THIN);  
						}
					}
				}
				begLine += count;
				
			}

            for (int i = 0; i < 8; i++) {
                sheet.autoSizeColumn(i, true); // 自动调整宽度
            }

			FileOutputStream fileOut = new FileOutputStream(file);
			wb.write(fileOut); 
			fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		//generateReport(null);
	}

}

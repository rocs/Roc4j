/**
 * 
 */
package perf.entry;


import me.roczh.roc4j.demo.test.Scenario;
import org.junit.AfterClass;
import org.junit.Test;
import perf.framework.POIUtil;
import perf.framework.ScenarioContext;

/**
 * 性能测试入口(以单元测试的形式)
 *  
 * @author Peng
 *
 */
public class ScenarioPerfTest {

    /**
     * Test method for
     * {@link Scenario#sort(int[])}.
     */
	@Test
	public void tstSort() {
		int number = 1;
		String description = "sort one array ascendingly";
		String method = "tstSort";
		ScenarioContext.equip(number, description, method);
	}

    /**
     * Test method for
     * {@link Scenario#getFilesCount(java.io.File)}
     */
	@Test
	public void tstGetFilesCount() {
		int number = 2;
		String description = "get total count of files in a directory";
		String method = "tstGetFilesCount";
		ScenarioContext.equip(number, description, method);
	}

    /**
     *  generate execel report via POI
     */
	@AfterClass
	public static void genReport(){
		POIUtil.generateReport(ScenarioContext.getResultRecords());
	}
}

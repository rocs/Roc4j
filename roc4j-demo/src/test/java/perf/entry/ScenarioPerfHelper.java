package perf.entry;

import junit.framework.TestCase;
import me.roczh.roc4j.demo.test.Scenario;

import java.io.File;
import java.util.List;
import java.util.Vector;

/**
 * 辅以性能测试的测试模板
 *
 * @author Peng
 */
public class ScenarioPerfHelper extends TestCase {


    private static List<Long> timeRecords = new Vector<Long>();
    public static List<Long> getTimeRecords() {
        return timeRecords;
    }
    public static void addTimeRecord(Long time){
        timeRecords.add(time);
    }
    public static void cleanTimeRecords() {
        timeRecords = new Vector<Long>();
    }

    public ScenarioPerfHelper(String name) {
        super(name);
    }

    private Scenario scenario;

    public void setUp() {
        scenario = new Scenario();

    }
    public void tearDown() {
    }

    /**
     * Test method for
     * {@link Scenario#sort(int[])}.
     */
   public void tstSort() {
        try {
            int[] array = {9, 0, -4, 2, 6, 1}, expectedArray = {-4, 0, 1, 2, 6, 9};

            long before = System.currentTimeMillis();

            int[] result = scenario.sort(array);

            long after = System.currentTimeMillis();
            long elapsedTime = after - before;
            timeRecords.add(elapsedTime);

            for (int i = 0; i < array.length; i++) {
                assertEquals(expectedArray[i], result[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test method for
     * {@link Scenario#getFilesCount(File)}
     */
    public void tstGetFilesCount() {
        File file = new File(".");

        long before = System.currentTimeMillis();

        int result = scenario.getFilesCount(file);

        long after = System.currentTimeMillis();
        long elapsedTime = after - before;
        timeRecords.add(elapsedTime);

        assertEquals(58, result);
    }
}

import com.jfinal.ext.test.ControllerTestCase;
import com.jfinal.i18n.Res;
import com.jfinal.kit.PathKit;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Created by Rocs on 2016/4/14.
 */
public class BlogControllerTestCase extends ControllerTestCase<ApplicationConfig> {

    @Test
    public void listGet() {
        String url = "/m/blog/list";
        String outFilePath = new File(PathKit.getRootClassPath()).getParent() + File.separator + "m_blog_list_get_resp.html";
        use(url).writeTo(new File(outFilePath)).invoke();
        Assert.assertEquals("Roc4j Demo", ((Res)findAttrAfterInvoke("i18n")).get("title"));
    }


    @Test
    public void randomSavePost() {
        String url = "/m/blog/randomSave?total=3";
        String outFilePath = new File(PathKit.getRootClassPath()).getParent() + File.separator + "m_blog_randomSave_post_resp.html";
        use(url).post("").writeTo(new File(outFilePath)).invoke();
        Assert.assertEquals("Roc4j示例", ((Res)findAttrAfterInvoke("i18n")).get("title"));
    }
}

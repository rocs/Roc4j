package sample;

import junit.framework.Assert;
import me.roczh.roc4j.demo.test.Scenario;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertEquals;

/** 
* 业务场景下生产代码下单元测试
*
* @author Peng
*
*/ 
public class ScenarioTest {

    private Scenario scenario;

    @Before
    public void setUp() {
        scenario = new Scenario();

    }
    @After
    public void tearDown() {
    }

    /**
     * Test method for
     * {@link me.rocs.dev.java.sample.Scenario#sort(int[])}.
     */
    @Test
    public void tstSort() {
        try {
            int[] array = {9, 0, -4, 2, 6, 1}, expectedArray = {-4, 0, 1, 2, 6, 9};
            int[] result = scenario.sort(array);
            for (int i = 0; i < array.length; i++) {
                assertEquals(expectedArray[i], result[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    /**
     * Test method for
     * {@link me.rocs.dev.java.sample.Scenario#getFilesCount(File)}
     */
    @Test
    public void tstGetFilesCount() {
        File file = new File(".");
        int result = scenario.getFilesCount(file);
        assertEquals(57, result);
    }
}

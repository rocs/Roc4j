import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import perf.entry.ScenarioPerfTest;

/**
 *
 * 业务场景下生产代码下测试套件入口
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ScenarioPerfTest.class})
public class _BootTotalTest {
}

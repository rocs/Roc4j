import com.jfinal.ext.test.ControllerTestCase;
import com.jfinal.plugin.config.ConfigKit;
import com.jfinal.plugin.config.ConfigPlugin;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peng on 2016/4/15.
 */
public class CommonTestCase extends ControllerTestCase<ApplicationConfig>{

    @Test
    public void testGetStr() throws InterruptedException {
        Assert.assertEquals("test", ConfigKit.getStr("a"));
    }

}

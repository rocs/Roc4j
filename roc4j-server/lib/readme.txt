core/
[javax.servlet] servlet-api-3.1.jar
com/jfinal/core/ActionException.java
com/jfinal/core/ActionHandler.java
com/jfinal/core/Controller.java
com/jfinal/core/JFinal.java
json/
[fastjson]  fastjson-1.2.6.jar    (*)
com/jfinal/json/FastJson.java
log/
[Log4j] log4j-1.2.17.jar    (*)  + slf4j-api-1.7.19.jar 、slf4j-log4j12-1.7.19.jar (应该)
com/jfinal/log/Log4jLog.java
upload/
cos-26Dec2008.jar   (可选)
com/jfinal/upload/MultipartRequest.java
com/jfinal/upload/OreillyCos.java
aop/
cglib-nodep-3.1.jar (应该)
com/jfinal/aop/Invocation.java
plugin/
[Active Record] javax.servlet.jsp-api-2.3.1.jar javax.el-api-3.0.0.jar  注意：ModelRecordElResolver.setResolveBeanAsModel方法调用时就成必须了
com/jfinal/plugin/activerecord/ModelRecordElResolver.java
plugin/
[EhCache]   ehcache-core-2.5.2.jar  (可选)
com/jfinal/plugin/ehcache/CacheKit.java
com/jfinal/plugin/ehcache/EhCachePlugin.java
plugin/
[Druid] druid-1.0.5.jar (可选)
com/jfinal/plugin/druid/DruidPlugin.java
plugin/
[Redis Client]  jedis-2.7.2.jar
com/jfinal/plugin/redis/Cache.java
com/jfinal/plugin/redis/RedisInterceptor.java
com/jfinal/plugin/redis/serializer/FstSerializer.java
[Serialize] fst-2.29.jar
com/jfinal/plugin/redis/serializer/FstSerializer.java
[Object Pool]   commons-pool2-2.3.jar
com/jfinal/plugin/redis/RedisPlugin.java
server/
[jetty]
jetty-webapp-9.2.15.v20160210.jar
jetty-util-9.2.15.v20160210.jar
com/jfinal/server/JFinalClassLoader.java
jetty-server-9.2.15.v20160210.jar
com/jfinal/server/JettyServer.java
[jetty 8.x->9.x & SSL]
jetty-servlet-9.2.15.v20160210.jar
com/jfinal/server/JettyServer.java
->com/jfinal/server/ServerFactory.java
->com/jfinal/core/JFinal.java

core/
antlr4-runtime-4.5.1-1.jar  (*)
org/beetl/core/parser/BeetlAntlrErrorStrategy.java
org/beetl/core/parser/BeetlParser.java
org/beetl/core/parser/SyntaxErrorListener.java
org/beetl/core/AntlrProgramBuilder.java

websocket/
[Java Websocket API]    javax.websocket-api-1.0.jar
server/
[jetty]
javax-websocket-server-impl-9.2.15.v20160210.jar
websocket-common-9.2.15.v20160210.jar
javax-websocket-client-impl-9.2.15.v20160210.jar
com/jfinal/server/JettyServer.java

[jetty's Runtime]
jetty-server-9.2.15.v20160210.jar
1)org.eclipse.jetty.server.Connector
jetty-util-9.2.15.v20160210.jar
2)org.eclipse.jetty.util.component.LifeCycle
jetty-servlet-9.2.15.v20160210.jar
3)org.eclipse.jetty.servlet.ServletContextHandler
jetty-webapp-9.2.15.v20160210.jar
4)org.eclipse.jetty.webapp.WebAppContext
javax.websocket-api-1.0.jar
5)javax.websocket.server.ServerContainer
servlet-api-3.1.jar
6)javax.servlet.http.HttpServletResponse
jetty-http-9.2.15.v20160210.jar
7)org.eclipse.jetty.http.HttpField
jetty-io-9.2.15.v20160210.jar
8)org.eclipse.jetty.io.ByteBufferPool
jetty-security-9.2.15.v20160210.jar
9)org.eclipse.jetty.security.SecurityHandler
javax-websocket-server-impl-9.2.15.v20160210.jar
10)org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer
websocket-server-9.2.15.v20160210.jar
11)org.eclipse.jetty.websocket.server.MappedWebSocketCreator
websocket-api-9.2.15.v20160210.jar
12)org.eclipse.jetty.websocket.api.WebSocketPolicy
websocket-servlet-9.2.15.v20160210.jar
13)org.eclipse.jetty.websocket.servlet.WebSocketCreator
websocket-common-9.2.15.v20160210.jar
14)org.eclipse.jetty.websocket.common.SessionListener
javax-websocket-client-impl-9.2.15.v20160210.jar
15)org.eclipse.jetty.websocket.jsr356.ClientContainer
websocket-client-9.2.15.v20160210.jar
16)org.eclipse.jetty.websocket.client.io.UpgradeListener
jetty-xml-9.2.15.v20160210.jar
17)org.eclipse.jetty.xml.XmlParser
jetty-servlets-9.2.15.v20160210.jar
18)org.eclipse.jetty.servlets.GzipFilter
com.jfinal.server.JettyServer:
Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
        "org.eclipse.jetty.annotations.AnnotationConfiguration");
19)jetty-annotations-9.2.15.v20160210.jar
20)jetty-plus-9.2.15.v20160210.jar
21)asm-5.0.1.jar
22)javax.annotation-api-1.2.jar

[ext]
guava-19.0.jar  (*)
ext/test/
junit-4.12.jar
com.jfinal.ext.test.ControllerTestCase
com.jfinal.ext.test.MockHttpRequest
plugin/
[Quartz]    quartz-2.2.1.jar
com.jfinal.plugin.quartz.QuartzPlugin
plugin/sqlmgr/
[Dom4j] dom4j-1.6.1.jar  (应该)
com.jfinal.plugin.sqlmgr.SqlKit
render/
[POI]   poi-3.11.jar    poi-ooxml-3.11.jar
com.jfinal.ext.render.excel.PoiRender
com.jfinal.ext.kit.excel.PoiExporter
com.jfinal.ext.kit.excel.PoiImporter

[view]
[YUI Compressor]    yuicompressor-2.4.8.jar commons-io-2.4.jar
me.roczh.roc4j.demo.view.kit.AssetsKit
package com.jfinal.websocket;

import com.jfinal.kit.JsonKit;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.util.Map;

/**
 * Created by peng on 2016/3/19.
 */
public class MessageDecoder implements Decoder.Text<Message> {

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public Message decode(String msg) throws DecodeException {
        Message message = MessageFactory.create(msg);
        if(message.valid()){
            return message;
        }else{
            throw new DecodeException(msg, "Invalid message type");
        }
    }

    @Override
    public boolean willDecode(String msg) {
        // Determine if the message can be converted
        if (JsonKit.parse(msg, Map.class).containsKey("msgcode")) {
            return true;
        }
        return false;
    }
}

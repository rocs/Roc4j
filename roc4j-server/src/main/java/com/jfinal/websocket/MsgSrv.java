package com.jfinal.websocket;


import com.jfinal.kit.JsonKit;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by peng on 2016/3/19.
 */
@ServerEndpoint(
        value = "/4msg/{clientId}",
        subprotocols={"msg-roc4j"},
        decoders = {MessageDecoder.class},
        encoders = {MessageEncoder.class},
        configurator = MessageServerConfigurator.class
)
public class MsgSrv {
    public static long SESSION_TIMEOUT = 20 * 60 * 1000;
    public static String KEY_CURRENT_USER_ID = "current-user-id";
    private static String SESSION2HTTPSESSION = "http-session";
    private static final ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    /* Queue for all open WebSocket sessions */
    private static Map<String, Queue<Session>> client2sessions = new ConcurrentHashMap<String, Queue<Session>>();
    private Session session;
    private HttpSession httpSession;

    @OnOpen
    public void open(Session session,
                     EndpointConfig config,
                     @PathParam("clientId") String clientId) {
        LogKit.info("Connection@" + session.getId() + " from " + session.getRequestURI() + "..");
        this.session = session;
        this.session.setMaxIdleTimeout(SESSION_TIMEOUT);
        this.httpSession = (HttpSession) config.getUserProperties().get(MessageServerConfigurator.KEY_HTTP_SESSION);
        this.session.getUserProperties().put(SESSION2HTTPSESSION, this.httpSession);
        String userId = null;
        try {
            if (!StrKit.isBlank(userId = (String)this.httpSession.getAttribute(KEY_CURRENT_USER_ID)) && clientId.equals(userId)) {
                this.session.getBasicRemote().sendObject(HelloMessage.create("Welcome " + userId + "@" + this.session.getId() + "." ));
                Queue<Session> queue = null;
                if(client2sessions.get(clientId) != null){
                    queue = client2sessions.get(clientId);
                }else{
                    queue = new LinkedList<Session>();
                }
                queue.offer(this.session);
                client2sessions.put(clientId, queue);
            }else{
                LogKit.error("Illegal Connection@" + this.session.getRequestURI() + "! ");
                this.session.close();
            }
        } catch (IOException e) {
            LogKit.error(e.getMessage(), e);
        } catch (EncodeException e) {
            LogKit.error(e.getMessage(), e);
        }
    }

    @OnMessage
    public void handleMessage(Message msg) {
        StringBuilder sb = new StringBuilder("\nmsg report <<<<<<<< ").append(sdf.get().format(new Date())).append(" ------------------------------\n");
        sb.append("Message : ").append(msg.getClass().getName()).append(".(").append(msg.getClass().getSimpleName()).append(".java:1)");
        sb.append("\nMsgcode : ").append(msg.getMsgcode());
        sb.append("\nData    : ").append(JsonKit.toJson(msg)).append("\n");
        sb.append("--------------------------------------------------------------------------------\n");
        System.out.print(sb.toString());
        msg.onReceived();
    }


    @OnError
    public void error(Throwable t) {
        if(t instanceof IllegalArgumentException || t instanceof DecodeException){
            LogKit.error(this.session.getId() + " Via " + this.session.getRequestURI() + " : " + t.getMessage());
        }else{
            LogKit.error(t.getMessage(), t);
        }
    }

    @OnClose
    public void close(CloseReason reason) {
     /* Remove this connection from the queue */
        removeSession(this.session);
        LogKit.info("Connection@" + session.getId() + "  close with reason: " + reason.toString() + ".");
    }

    public void removeSession(Session session) {
        Collection<Queue<Session>> sessions = MsgSrv.client2sessions.values();
        for(Queue<Session> queue : sessions){
            if(queue.contains(session)){
                queue.remove(session);
            }
        }
    }

    public void removeSession(String clientId) {
        client2sessions.remove(clientId);
    }

    public static void send(Message message) {
        send(message, true);
    }
    public static void send(Message message, boolean sync) {
        String receiver = message.getTouser();
        if (StrKit.isBlank(receiver)) {
            System.err.println("Receiver is Blank!");
            return;
        }
        try {
            /* Send updates to the open WebSocket client2sessions */
            Queue<Session> queue = client2sessions.get(receiver);
            Session session = null;
            Iterator<Session> iterator = queue.iterator();
            while (iterator.hasNext()){
                session = iterator.next();
                if(sync){
                    session.getBasicRemote().sendObject(message);
                }else {
                    session.getAsyncRemote().sendObject(message);
                }
                StringBuilder sb = new StringBuilder("\nmsg report >>>>>>>> ").append(sdf.get().format(new Date())).append(" ------------------------------\n");
                sb.append("Message : ").append(message.getClass().getName()).append(".(").append(message.getClass().getSimpleName()).append(".java:1)");
                sb.append("\nReceiver: ").append(session.getId());
                sb.append("\nData    : ").append(JsonKit.toJson(message)).append("\n");
                sb.append("--------------------------------------------------------------------------------\n");
                System.out.print(sb.toString());
            }
        } catch (IOException e) {
            LogKit.error(e.getMessage(), e);
        } catch (EncodeException e) {
            LogKit.error(e.getMessage(), e);
        }
    }

    public static void sendAll(Message message) {
        sendAll(message, true);
    }
    public static void sendAll(Message message, boolean sync) {
        try {
            /* Send updates to all open WebSocket client2sessions */
            Collection<Queue<Session>> queues = client2sessions.values();
            for (Queue<Session> queue : queues) {
                Session session = null;
                Iterator<Session> iterator = queue.iterator();
                while (iterator.hasNext()){
                    session = iterator.next();
                    if(sync){
                        session.getBasicRemote().sendObject(message);
                    }else {
                        session.getAsyncRemote().sendObject(message);
                    }
                    StringBuilder sb = new StringBuilder("\nmsg report >>>>>>>> ").append(sdf.get().format(new Date())).append(" ------------------------------\n");
                    sb.append("Message : ").append(message.getClass().getName()).append(".(").append(message.getClass().getSimpleName()).append(".java:1)");
                    sb.append("\n@all    : ").append(session.getId());
                    sb.append("\nData    : ").append(JsonKit.toJson(message)).append("\n");
                    sb.append("--------------------------------------------------------------------------------\n");
                    System.out.print(sb.toString());
                }
            }
        } catch (IOException e) {
            LogKit.error(e.getMessage(), e);
        } catch (EncodeException e) {
            LogKit.error(e.getMessage(), e);
        }
    }

}

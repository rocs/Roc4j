package com.jfinal.websocket;

/**
 * Created by peng on 2016/3/19.
 */
public abstract class Message {

  private int msgcode;

  private String touser;

  private String fromuser;

  private long createtime;

  private String msgid;

  public int getMsgcode() {
    return msgcode;
  }

  public void setMsgcode(int msgcode) {
    this.msgcode = msgcode;
  }

  public String getTouser() {
    return touser;
  }

  public void setTouser(String touser) {
    this.touser = touser;
  }

  public String getFromuser() {
    return fromuser;
  }

  public void setFromuser(String fromuser) {
    this.fromuser = fromuser;
  }

  public long getCreatetime() {
    return createtime;
  }

  public void setCreatetime(long createtime) {
    this.createtime = createtime;
  }

  public String getMsgid() {
    return msgid;
  }

  public void setMsgid(String msgid) {
    this.msgid = msgid;
  }

  public boolean valid(){ return true;}
  public void onReceived(){};

}

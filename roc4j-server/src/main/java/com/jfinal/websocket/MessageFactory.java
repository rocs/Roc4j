package com.jfinal.websocket;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.LogKit;

import javax.websocket.DecodeException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rocs on 2016/3/19.
 */
public class MessageFactory {

    public static  Map<Integer, Class<? extends Message>> SUPPORED_MSG_CODES = new HashMap<Integer, Class<? extends Message>>();
    static{
        LogKit.debug(MessageFactory.class.getClassLoader().toString());
        MessageFactory.SUPPORED_MSG_CODES.put(HelloMessage.MSG_CODE, HelloMessage.class);
    }

    public static void registerMessage(int msgcode, Class<? extends Message> messageClazz){
        MessageFactory.SUPPORED_MSG_CODES.put(msgcode, messageClazz);
    }

    public static Message create(String jsonStr) throws DecodeException{
        Map map  = JsonKit.parse(jsonStr, Map.class);
        try {
            if (map.containsKey("msgcode")) {
                int msgcode = (int) map.get("msgcode");
                System.out.println(MessageFactory.SUPPORED_MSG_CODES);
                if(MessageFactory.SUPPORED_MSG_CODES.containsKey(msgcode)){
                    return JsonKit.parse(jsonStr, MessageFactory.SUPPORED_MSG_CODES.get(msgcode));
                }else{
                    throw new DecodeException(jsonStr, "Unknown message type");
                }
            }else{
                throw new IllegalArgumentException("Illegal message");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Illegal message: " + e.getLocalizedMessage());
        }
    }
}

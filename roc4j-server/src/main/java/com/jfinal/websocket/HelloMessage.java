package com.jfinal.websocket;

/**
 * Created by Rocs on 2016/3/19.
 */
public class HelloMessage extends Message {

    public static final int MSG_CODE = 1;

    @Override
    public int getMsgcode() {
        return MSG_CODE;
    }

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static HelloMessage create(String content){
        HelloMessage message = new HelloMessage();
        message.setContent(content);
        message.setCreatetime(System.currentTimeMillis());
        return message;
    }
}

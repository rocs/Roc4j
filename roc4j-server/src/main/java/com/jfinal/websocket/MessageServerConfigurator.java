package com.jfinal.websocket;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * Created by peng on 2016/3/19.
 */
public class MessageServerConfigurator extends ServerEndpointConfig.Configurator {

    public static String KEY_HTTP_SESSION = "http-session";

    @Override
    public void modifyHandshake(ServerEndpointConfig conf,
                                HandshakeRequest req,
                                HandshakeResponse resp) {
        HttpSession httpSession = (HttpSession) req.getHttpSession();
        conf.getUserProperties().put(KEY_HTTP_SESSION, req.getHttpSession());
    }

}
package com.jfinal.websocket;

import com.jfinal.kit.JsonKit;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;


/**
 * Created by peng on 2016/3/19.
 */
public class MessageEncoder implements Encoder.Text<Message> {
    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public String encode(Message msg) throws EncodeException {
        return JsonKit.toJson(msg);
    }
}
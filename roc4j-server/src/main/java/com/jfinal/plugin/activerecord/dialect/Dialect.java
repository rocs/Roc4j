/**
 * Copyright (c) 2011-2016, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfinal.plugin.activerecord.dialect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.jfinal.kit.LogKit;
import com.jfinal.plugin.activerecord.*;

/**
 * Dialect.
 */
public abstract class Dialect {
	
	// Methods for common
	public abstract String forTableBuilderDoBuild(String tableName);
	public abstract String forPaginate(int pageNumber, int pageSize, String select, String sqlExceptSelect);
	
	// Methods for Model
	public abstract String forModelFindById(Table table, String columns);
	public abstract String forModelDeleteById(Table table);
	public abstract void forModelSave(Table table, Map<String, Object> attrs, StringBuilder sql, List<Object> paras);
	public abstract void forModelUpdate(Table table, Map<String, Object> attrs, Set<String> modifyFlag, StringBuilder sql, List<Object> paras);
	
	// Methods for DbPro. Do not delete the String[] pKeys parameter, the element of pKeys needs to trim()
	public abstract String forDbFindById(String tableName, String[] pKeys);
	public abstract String forDbDeleteById(String tableName, String[] pKeys);
	public abstract void forDbSave(String tableName, String[] pKeys, Record record, StringBuilder sql, List<Object> paras);
	public abstract void forDbUpdate(String tableName, String[] pKeys, Object[] ids, Record record, StringBuilder sql, List<Object> paras);
	
	public boolean isOracle() {
		return false;
	}
	
	public boolean isTakeOverDbPaginate() {
		return false;
	}
	
	public Page<Record> takeOverDbPaginate(Connection conn, int pageNumber, int pageSize, Boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) throws SQLException {
		throw new RuntimeException("You should implements this method in " + getClass().getName());
	}
	
	public boolean isTakeOverModelPaginate() {
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	public Page takeOverModelPaginate(Connection conn, Class<? extends Model> modelClass, int pageNumber, int pageSize, Boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) throws Exception {
		throw new RuntimeException("You should implements this method in " + getClass().getName());
	}
	
	public void fillStatement(PreparedStatement pst, List<Object> paras) throws SQLException {
		int size = paras.size();
		boolean isShowSql = DbKit.getConfig().isShowSql();

		StringBuilder sb = null;
		if(isShowSql){
			sb = new StringBuilder();
			sb.append("\r\n [SQL's param: ").append((size == 0 ? " empty " :  size)).append("] \r\n ");
		}

		for (int i = 0; i<size; i++) {
			int paramIndex = i + 1;
			Object paramObject = paras.get(i);
			pst.setObject(paramIndex, paramObject);

			if(isShowSql){
				sb.append("param index: ").append(paramIndex)
						.append("   param type: ").append((null != paramObject ? paramObject.getClass().getSimpleName() : "null"))
						.append("   param value: ").append(String.valueOf(paramObject)).append(" \r\n ");
			}
		}

		if(isShowSql){
			LogKit.info(sb.toString());
		}
	}
	
	public void fillStatement(PreparedStatement pst, Object... paras) throws SQLException {
		int size = paras.length;
		boolean isShowSql = DbKit.getConfig().isShowSql();

		StringBuilder sb = null;
		if(isShowSql){
			sb = new StringBuilder();
			sb.append("\r\n [SQL's param: ").append((size == 0 ? " empty " :  size)).append("] \r\n ");
		}

		for (int i=0; i<size; i++) {
			int paramIndex = i + 1;
			Object paramObject = paras[i];
			pst.setObject(paramIndex, paramObject);

			if(isShowSql){
				sb.append("param index: ").append(paramIndex)
						.append("   param type: ").append((null != paramObject ? paramObject.getClass().getSimpleName() : "null"))
						.append("   param value: ").append(String.valueOf(paramObject)).append(" \r\n ");
			}
		}

		if(isShowSql){
			LogKit.info(sb.toString());
		}
	}
	
	public String getDefaultPrimaryKey() {
		return "id";
	}
	
	public boolean isPrimaryKey(String colName, String[] pKeys) {
		for (String pKey : pKeys) {
			if (colName.equalsIgnoreCase(pKey)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 一、forDbXxx 系列方法中若有如下两种情况之一，则需要调用此方法对 pKeys 数组进行 trim():
	 * 1：方法中调用了 isPrimaryKey(...)：为了防止在主键相同情况下，由于前后空串造成 isPrimaryKey 返回 false
	 * 2：为了防止 tableName、colName 与数据库保留字冲突的，添加了包裹字符的：为了防止串包裹区内存在空串
	 *   如 mysql 使用的 "`" 字符以及 PostgreSql 使用的 "\"" 字符
	 * 不满足以上两个条件之一的 forDbXxx 系列方法也可以使用 trimPrimaryKeys(...) 方法让 sql 更加美观，但不是必须
	 * 
	 * 二、forModelXxx 由于在映射时已经trim()，故不再需要调用此方法
	 */
	public void trimPrimaryKeys(String[] pKeys) {
		for (int i=0; i<pKeys.length; i++) {
			pKeys[i] = pKeys[i].trim();
		}
	}
	
	protected static class Holder {
		// "order\\s+by\\s+[^,\\s]+(\\s+asc|\\s+desc)?(\\s*,\\s*[^,\\s]+(\\s+asc|\\s+desc)?)*";
		private static final Pattern ORDER_BY_PATTERN = Pattern.compile(
			"order\\s+by\\s+[^,\\s]+(\\s+asc|\\s+desc)?(\\s*,\\s*[^,\\s]+(\\s+asc|\\s+desc)?)*",
			Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
	}
	
	public String replaceOrderBy(String sql) {
		return Holder.ORDER_BY_PATTERN.matcher(sql).replaceAll("");
	}


	private String columnVersion = "version";	//乐观锁版本号	bigint
	private String columnCreateTime = "create_time";	//记录创建时间	timestamp
	private String columnUpdateTime = "update_time";	//记录更新时间	timestamp
	private String columnDeleteTag = "disable";	//逻辑删除标记位

	public String getColumnVersion() {
		return columnVersion;
	}

	public String getColumnCreateTime() {
		return columnCreateTime;
	}

	public String getColumnUpdateTime() {
		return columnUpdateTime;
	}

	public String getColumnDeleteTag() {
		return columnDeleteTag;
	}

	public Dialect setColumnVersion(String columnVersion) {
		this.columnVersion = columnVersion;
		return this;
	}

	public Dialect setColumnCreateTime(String columnCreateTime) {
		this.columnCreateTime = columnCreateTime;
		return this;
	}

	public Dialect setColumnUpdateTime(String columnUpdateTime) {
		this.columnUpdateTime = columnUpdateTime;
		return this;
	}

	public Dialect setColumnDeleteTag(String columnDeleteTag) {
		this.columnDeleteTag = columnDeleteTag;
		return this;
	}
}







package com.jfinal.plugin.sqlmgr;

import com.jfinal.plugin.IPlugin;

/**
 * 加载sql文件
 * @author 董华健
 */
public class SqlManagerPlugin implements IPlugin {

    public SqlManagerPlugin() {
    }

    @Override
    public boolean start() {
        SqlKit.initSQLs(true);
        return true;
    }

    @Override
    public boolean stop() {
        SqlKit.clearSQLs();
        return true;
    }

}

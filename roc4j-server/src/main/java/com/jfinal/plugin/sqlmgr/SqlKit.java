package com.jfinal.plugin.sqlmgr;

import com.jfinal.kit.LogKit;
import org.beetl.core.BeetlKit;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 处理Sql Map
 * @author 董华健
 * 说明：加载sql map中的sql到map中，并提供动态长度sql处理
 */
public class SqlKit {

    /**
     * xml中所有的sql语句
     */
    private static final ConcurrentMap<String, String> sqlMap = new ConcurrentHashMap<>();

    /**
     * 获取SQL，固定SQL
     * @param sqlKey
     * @return
     */
    public static String get(String sqlKey) {
    	String sql = sqlMap.get(sqlKey);
    	if(null == sql || sql.isEmpty()){
			LogKit.error("sql语句不存在：sql id是 " + sqlKey + " ");
    	}
    	
        return sql.replaceAll("[\\s]{2,}", " ");
    }

    /**
     * 获取SQL，动态SQL
     * @param sqlKey
     * @param param 查询参数
     * @param paramValueList 用于接收预处理的值
     * @return
     */
    public static String get(String sqlKey, Map<String, String> param, LinkedList<Object> paramValueList) {
    	String sqlTemplete = sqlMap.get(sqlKey);
    	if(null == sqlTemplete || sqlTemplete.isEmpty()){
			LogKit.error("sql语句不存在：sql id是 " + sqlKey + " ");
    	}
    	
    	Map<String, Object> paramMap = new HashMap<String, Object>();
    	Set<String> paramKeySet = param.keySet();
    	for (String paramKey : paramKeySet) {
    		paramMap.put(paramKey, (Object)param.get(paramKey));
		}
    	String sql = BeetlKit.render(sqlTemplete, paramMap);
		
    	Pattern pattern = Pattern.compile("#[\\w\\d\\$\\'\\%\\_]+#");	//#[\\w\\d]+#    \\$
		Pattern pattern2 = Pattern.compile("\\$[\\w\\d\\_]+\\$");
		
		Matcher matcher = pattern.matcher(sql);
		
		while (matcher.find()) {
			String clounm = matcher.group(0); // 得到的结果形式：#'%$names$%'#
			
			Matcher matcher2 = pattern2.matcher(clounm);
			matcher2.find();
			String clounm2 = matcher2.group(0); // 得到的结果形式：$names$
			
			String clounm3 = clounm2.replace("$", "");
			
			if(clounm.equals("#" + clounm2 + "#")){ // 数值型，可以对应处理int、long、bigdecimal、double等等
				String val = (String) param.get(clounm3);
				try {
					Integer.parseInt(val);
					sql = sql.replace(clounm, val);
				} catch (NumberFormatException e) {
					LogKit.error("查询参数值错误，整型值传入了字符串，非法字符串是：" + val);
					return null;
				}
				
			}else{ // 字符串，主要是字符串模糊查询、日期比较的查询
				String val = (String) param.get(clounm3);
				
				String clounm4 = clounm.replace("#", "").replace("'", "").replace(clounm2, val);
				paramValueList.add(clounm4);
				
				sql = sql.replace(clounm, "?");
			}
		}

        return sql.replaceAll("[\\s]{2,}", " ");
    }
    
    /**
     * 清除所加载的sql语句
     */
    public static void clearSQLs() {
        sqlMap.clear();
    }

    /**
     * 初始化加载sql语句到sqlMap
     */
	public static synchronized void initSQLs(boolean isInit) {
		String classRootPath = SqlKit.class.getClassLoader().getResource("").getFile();
		try {
			classRootPath = java.net.URLDecoder.decode(classRootPath, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			LogKit.error("初始化加载sql配置：获取classRootPath异常!");
		}
        File file = new File(classRootPath);
    	List<File> files = new ArrayList<File>();
        findFiles(file, files);
        
    	SAXReader reader = new SAXReader();
    	String fileName = null;
    	try {
	        for (File xmlfile : files) {
	        	fileName = xmlfile.getName();
				Document doc = reader.read(xmlfile);
				Element root = doc.getRootElement();
				String namespace = root.attributeValue("namespace");
				if(null == namespace || namespace.trim().isEmpty()){
					LogKit.error("sql xml文件 " + fileName + " 的命名空间不能为空");
					continue;
				}
				
				for(Iterator<?> iterTemp = root.elementIterator(); iterTemp.hasNext();) {	
					Element element = (Element) iterTemp.next();	
					if(element.getName().toLowerCase().equals("sql")){
						String id = element.attributeValue("id");
						if(null == id || id.trim().isEmpty()){
							LogKit.error("sql xml文件 " + fileName + " 的存在没有id的sql语句");
							continue;
						}
						
						String sql = element.getText();
						if(null == sql || sql.trim().isEmpty()){
							LogKit.error("sql xml文件 " + fileName + " 的存在没有内容的sql语句");
							continue;
						}
						
						String key = namespace + "." + id;
						if(isInit && sqlMap.containsKey(key)){
							LogKit.error("sql xml文件 " + fileName + " 的sql语句 " + key + " 的存在重复命名空间和ID");
							continue;
						} else if(sqlMap.containsKey(key)){
							LogKit.error("sql xml文件 " + fileName + " 的sql语句 " + key + " 的存在重复命名空间和ID");
						}
						
						sql = sql.replaceAll("[\\s]{2,}", " ");
						sqlMap.put(key, sql);
						LogKit.debug("sqlMap.put(file= " + fileName + " , key= " + key + " , content= " + sql +" )");
					}
				}
	        }
		} catch (DocumentException e) {
			LogKit.error("sql xml文件 " + fileName + " 解析异常");
			e.printStackTrace();
		}
    }
    
    /**
     * 递归查找文件
     * @param baseFile
     * @param sqlXmlFiles
     */
    private static void findFiles(File baseFile, List<File> sqlXmlFiles) {
        if (!baseFile.isDirectory()) {
        	if (baseFile.getName().endsWith(".sql.xml")) {
        		sqlXmlFiles.add(baseFile);
        	}
        } else {
            File[] fileList = baseFile.listFiles();
            for (File file : fileList) {
            	if (file.isDirectory()) {
            		findFiles(file, sqlXmlFiles);
                    
            	} else {
                	if (file.getName().endsWith(".sql.xml")) {
                		sqlXmlFiles.add(file);
                	}
                }
			}
        }
    }
    
}

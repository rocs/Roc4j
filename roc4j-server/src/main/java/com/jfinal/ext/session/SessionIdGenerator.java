package com.jfinal.ext.session;

/**
 * session id 生成器
 */
public interface SessionIdGenerator {

    /**
     * 生成session id
     *
     * @return
     */
    public String genaeratorSessionId();

}

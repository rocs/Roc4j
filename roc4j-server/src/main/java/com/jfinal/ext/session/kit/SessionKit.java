package com.jfinal.ext.session.kit;

import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.SessionManager;

/**
 * session操作工具类，提供sessionManager的静态访问接口
 */
public class SessionKit {

    private static SessionManager sessionManager;

    public static SessionManager getSessionManager() {
        return sessionManager;
    }

    public static void setSessionManager(SessionManager sessionManager) {
        SessionKit.sessionManager = sessionManager;
    }

    public static Session getSession(String sessiongId) {
        return sessionManager.getSession(sessiongId);
    }

    public static Session getNewSession() {
        return sessionManager.getNewSession();
    }

    public static void addSession(Session session) {
        sessionManager.addSession(session);
    }

    public static void removeSession(String sessionId) {
        sessionManager.removeSession(sessionId);
    }

    public static String getSessionIdKey() {
        return sessionManager.getSessionIdKey();
    }

    public static long getSessionTimeout() {
        return sessionManager.getSessionTimeout();
    }

}

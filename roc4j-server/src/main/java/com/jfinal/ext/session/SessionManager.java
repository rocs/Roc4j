package com.jfinal.ext.session;

/**
 * session管理器接口
 */
public interface SessionManager {

    /**
     * 添加一个session
     *
     * @param session
     */
    public void addSession(Session session);

    /**
     * 移除一个session
     *
     * @param sessionId
     */
    public void removeSession(String sessionId);

    /**
     * 根据session id获取一个session
     *
     * @param sessionId id
     * @return
     */
    public Session getSession(String sessionId);

    /**
     * 根据session id获取一个session
     *
     * @return
     */
    public Session getNewSession();

    /**
     * 根据session id刷新一个session
     *
     * @param session
     * @return
     */
    public void refreshSession(Session session);

    /**
     * 获取sessionId名称key
     *
     * @return
     */
    public String getSessionIdKey();

    /**
     * 设置sessionId名称key
     *
     * @param sessionIdKey
     */
    public void setSessionIdKey(String sessionIdKey);

    /**
     * 获取session超时时间
     *
     * @return
     */
    public int getSessionTimeout();

    /**
     * 设置session超时时间
     *
     * @param sessionTimeout
     */
    public void setSessionTimeout(int sessionTimeout);

    /**
     * 获取sessionId生成器
     *
     * @return
     */
    public SessionIdGenerator getSessionIdGenerator();

    /**
     * 设置sessionId生成器
     *
     * @param sessionIdGenerator
     */
    public void setSessionIdGenerator(SessionIdGenerator sessionIdGenerator);

    /**
     * 获取session存储器
     *
     * @return
     */
    public SessionDao getSessionDao();

    /**
     * 设置session存储器
     *
     * @param sessionDao
     */
    public void setSessionDao(SessionDao sessionDao);

}

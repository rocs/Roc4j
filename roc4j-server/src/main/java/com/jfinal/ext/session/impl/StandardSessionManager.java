package com.jfinal.ext.session.impl;

import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.SessionDao;
import com.jfinal.ext.session.SessionIdGenerator;
import com.jfinal.ext.session.SessionManager;
import com.jfinal.kit.StrKit;

/**
 * 自定义session管理器
 */
public class StandardSessionManager implements SessionManager {
    private String sessionIdKey = "_SESSIONID"; //session id key，默认为_SESSIONID
    private int sessionTimeout = 30 * 60 * 1000; //session过期时间，默认值为30分钟
    private SessionIdGenerator sessionIdGenerator; //session id 生成器，默认使用uuid生成策略
    private SessionDao sessionDao; //session存储器，默认使用本地session管理

    public StandardSessionManager() {
        this.sessionIdGenerator = new UUIDSessionIdGenerator();
        this.sessionDao = new LocalSessionDao();
    }

    public StandardSessionManager(SessionDao sessionDao) {
        this.sessionIdGenerator = new UUIDSessionIdGenerator();
        this.sessionDao = sessionDao;
    }

    public StandardSessionManager(SessionIdGenerator sessionIdGenerator) {
        this.sessionIdGenerator = sessionIdGenerator;
        this.sessionDao = new LocalSessionDao();
    }

    public StandardSessionManager(SessionIdGenerator sessionIdGenerator, SessionDao sessionDao) {
        this.sessionIdGenerator = sessionIdGenerator;
        this.sessionDao = sessionDao;
    }

    public StandardSessionManager(SessionDao sessionDao, SessionIdGenerator sessionIdGenerator) {
        this(sessionIdGenerator, sessionDao);
    }

    @Override
    public Session getSession(String sessionId) {
        if (StrKit.isBlank(sessionId)) return null;
        Session session = this.sessionDao.getSession(sessionId);
        if (session == null) return null;
        if (session.isInvalidate()) {
            this.sessionDao.deleteSession(sessionId);
            return null;
        }
        session.setLastAccessedTime(System.currentTimeMillis());
        this.sessionDao.refreshSession(session);
        return session;
    }

    @Override
    public Session getNewSession() {
        return new StandardSession(this.sessionIdGenerator.genaeratorSessionId(), this.sessionTimeout);
    }

    @Override
    public void addSession(Session session) {
        if (session == null) return;
        this.sessionDao.saveSession(session);
    }

    @Override
    public void removeSession(String sessionId) {
        if (StrKit.isBlank(sessionId)) return;
        this.sessionDao.deleteSession(sessionId);
    }

    @Override
    public void refreshSession(Session session) {
        this.sessionDao.refreshSession(session);
    }

    @Override
    public String getSessionIdKey() {
        return sessionIdKey;
    }

    @Override
    public void setSessionIdKey(String sesssionIdKey) {
        this.sessionIdKey = sesssionIdKey;
    }

    @Override
    public int getSessionTimeout() {
        return sessionTimeout;
    }

    @Override
    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    @Override
    public SessionIdGenerator getSessionIdGenerator() {
        return sessionIdGenerator;
    }

    @Override
    public void setSessionIdGenerator(SessionIdGenerator sessionIdGenerator) {
        this.sessionIdGenerator = sessionIdGenerator;
    }

    @Override
    public SessionDao getSessionDao() {
        return sessionDao;
    }

    @Override
    public void setSessionDao(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }
}
package com.jfinal.ext.session;

/**
 * session存储管理器
 */
public interface SessionDao {

    /**
     * 保存一个session
     *
     * @param session
     * @return
     */
    public void saveSession(Session session);

    /**
     * 删除一个session
     *
     * @param sessionId
     */
    public void deleteSession(String sessionId);

    /**
     * 根据session id获取一个session
     *
     * @param sessionId
     * @return
     */
    public Session getSession(String sessionId);

    /**
     * 根据session id刷新一个session
     *
     * @param session
     * @return
     */
    public void refreshSession(Session session);

}

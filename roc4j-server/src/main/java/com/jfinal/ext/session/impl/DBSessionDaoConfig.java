package com.jfinal.ext.session.impl;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;


/**
 * db session dao 所需环境配置
 */
public class DBSessionDaoConfig {

    private String dbConfigName; //jfinal配置数据源名称
    private String tableName = "tbl_session"; // 存放session的表名
    private String sessionIdColumnName = "si"; //存放sessionId的列名
    private String sessionObjColumnName = "so"; //存放sessionObj对象的列名
    private String sessionLastActiveTimeColumnName = "lat"; //存放最后使用时间列名

    /**
     * 获取jfinalDb操作类
     *
     * @return
     */
    public DbPro getDbPro() {
        if (StrKit.isBlank(this.dbConfigName)) {
            return DbPro.use();
        }
        return Db.use(this.dbConfigName);
    }

    public String getDbConfigName() {
        return dbConfigName;
    }

    public void setDbConfigName(String dbConfigName) {
        if (!StrKit.isBlank(dbConfigName)) {
            this.dbConfigName = dbConfigName;
        }
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        if (!StrKit.isBlank(tableName)) {
            this.tableName = tableName;
        }
    }

    public String getSessionIdColumnName() {
        return sessionIdColumnName;
    }

    public void setSessionIdColumnName(String sessionIdColumnName) {
        if (!StrKit.isBlank(sessionIdColumnName)) {
            this.sessionIdColumnName = sessionIdColumnName;
        }
    }

    public String getSessionObjColumnName() {
        return sessionObjColumnName;
    }

    public void setSessionObjColumnName(String sessionObjColumnName) {
        if (!StrKit.isBlank(sessionObjColumnName)) {
            this.sessionObjColumnName = sessionObjColumnName;
        }
    }

    public String getSessionLastActiveTimeColumnName() {
        return sessionLastActiveTimeColumnName;
    }

    public void setSessionLastActiveTimeColumnName(String sessionLastActiveTimeColumnName) {
        this.sessionLastActiveTimeColumnName = sessionLastActiveTimeColumnName;
    }
}
package com.jfinal.ext.session.plugin;

import com.jfinal.ext.session.SessionManager;
import com.jfinal.ext.session.kit.SessionKit;
import com.jfinal.plugin.IPlugin;

/**
 * session管理插件
 */
public class SessionManagerPlugin implements IPlugin {

    private static SessionManager sessionManager;

    public SessionManagerPlugin(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    public boolean start() {
        SessionKit.setSessionManager(sessionManager);
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }

}

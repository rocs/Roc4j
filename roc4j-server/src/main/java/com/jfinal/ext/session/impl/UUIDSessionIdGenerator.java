package com.jfinal.ext.session.impl;


import com.jfinal.ext.session.SessionIdGenerator;

import java.util.UUID;

/**
 * 默认session id生成器：采用uuid
 */
public class UUIDSessionIdGenerator implements SessionIdGenerator {

    @Override
    public String genaeratorSessionId() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }

}

package com.jfinal.ext.session.impl;

import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.SessionDao;
import com.jfinal.plugin.activerecord.Record;
import org.apache.log4j.Logger;

import java.io.*;

/**
 * 基于db的session存储实现
 */
public class DBSessionDao implements SessionDao {

    protected static final Logger log = Logger.getLogger(DBSessionDao.class);

    private DBSessionDaoConfig config;

    public DBSessionDao() {
        this.config = new DBSessionDaoConfig();
    }

    public DBSessionDao(DBSessionDaoConfig sessionDBConfig) {
        this.config = sessionDBConfig;
    }

    @Override
    public void saveSession(Session session) {
        Record sessionRecord = new Record();
        sessionRecord.set(this.config.getSessionIdColumnName(), session.getId());
        sessionRecord.set(this.config.getSessionObjColumnName(), toByteArray(session));
        sessionRecord.set(this.config.getSessionLastActiveTimeColumnName(), System.currentTimeMillis());
        this.config.getDbPro().save(this.config.getTableName(), sessionRecord);
    }

    @Override
    public void deleteSession(String sessionId) {
        this.config.getDbPro().deleteById(this.config.getTableName(), this.config.getSessionIdColumnName(), sessionId);
    }

    @Override
    public Session getSession(String sessionId) {
        Session session = null;
        Record sessionRecord = this.config.getDbPro().findById(this.config.getTableName(), this.config.getSessionIdColumnName(), sessionId);
        if (sessionRecord == null) return null;
        byte[] sessionBytes = sessionRecord.getBytes(this.config.getSessionObjColumnName());
        session = (Session)toObject(sessionBytes);
        session.setLastAccessedTime(sessionRecord.getLong(this.config.getSessionLastActiveTimeColumnName()));
        return session;
    }

    @Override
    public void refreshSession(final Session session) {
        Record sessionRecord = new Record();
        sessionRecord.set(this.config.getSessionIdColumnName(), session.getId());
        sessionRecord.set(this.config.getSessionLastActiveTimeColumnName(), System.currentTimeMillis());
        this.config.getDbPro().update(this.config.getTableName(), this.config.getSessionIdColumnName(), sessionRecord);
    }

    /**
     * 对象转字节数组
     * @param obj
     * @return  bytes
     */
    private byte[] toByteArray(Object obj) {
        byte[] bytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray ();
            oos.close();
            bos.close();
        } catch (IOException ex) {
            log.error(this, ex);
        }
        return bytes;
    }

    /**
     * 字节数组转对象
     * @param bytes
     * @return  obj
     */
    private Object toObject(byte[] bytes) {
        Object obj = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream (bytes);
            ObjectInputStream ois = new ObjectInputStream (bis);
            obj = ois.readObject();
            ois.close();
            bis.close();
        } catch (IOException ex) {
            log.error(this, ex);
        } catch (ClassNotFoundException ex) {
            log.error(this, ex);
        }
        return obj;
    }

}

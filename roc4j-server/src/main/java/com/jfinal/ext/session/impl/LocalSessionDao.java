package com.jfinal.ext.session.impl;


import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.SessionDao;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 基于本地容器的session存储实现
 */
public class LocalSessionDao implements SessionDao {

    //session容器
    private static Map<String, Session> sessionMap = new ConcurrentHashMap<String, Session>();

    @Override
    public void saveSession(Session session) {
        if (null != session) {
            sessionMap.put(session.getId(), session);
        }
    }

    @Override
    public void deleteSession(String sessionId) {
        if (null != sessionId && sessionId.length() > 0) {
            sessionMap.remove(sessionId);
        }
    }

    @Override
    public Session getSession(String sessionId) {
        if (null != sessionId && sessionId.length() > 0) {
            return sessionMap.get(sessionId);
        }
        return null;
    }

    @Override
    public void refreshSession(Session session) {
        sessionMap.put(session.getId(), session);
    }
}

package com.jfinal.ext.session.impl;

import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.SessionDao;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.plugin.redis.RedisPlugin;

/**
 * 基于redis的session存储实现
 */
public class RedisSessionDao implements SessionDao {

    //redis session cache
    private Cache sessionCache;

    public RedisSessionDao(String cacheName, String host) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, int port) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, port);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, int port, int timeout) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, port, timeout);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, int port, int timeout, String password) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, port, timeout, password);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, int port, int timeout, String password, int database) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, port, timeout, password, database);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, int port, int timeout, String password, int database, String clientName) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, port, timeout, password, database, clientName);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, int port, String password) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, port, password);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    public RedisSessionDao(String cacheName, String host, String password) {
        RedisPlugin redisPluginSession = new RedisPlugin(cacheName, host, password);
        redisPluginSession.start();
        this.sessionCache = Redis.use(cacheName);
    }

    @Override
    public void saveSession(Session session) {
        this.sessionCache.setex(session.getId(), Integer.parseInt(PropKit.get("session.maxAge")), session);
    }

    @Override
    public void deleteSession(String sessionId) {
        this.sessionCache.del(sessionId);
    }

    @Override
    public Session getSession(String sessionId) {
        return this.sessionCache.get(sessionId);
    }

    @Override
    public void refreshSession(Session session) {
        this.deleteSession(session.getId());
        this.saveSession(session);
    }

}

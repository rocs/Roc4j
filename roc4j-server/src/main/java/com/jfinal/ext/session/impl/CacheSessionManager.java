package com.jfinal.ext.session.impl;


import com.jfinal.ext.session.Session;
import com.jfinal.ext.session.SessionDao;
import com.jfinal.ext.session.SessionIdGenerator;
import com.jfinal.kit.StrKit;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 添加了二级缓存的session管理器
 */
public class CacheSessionManager extends StandardSessionManager {

    //本地二级缓存
    private Map<String, Session> cacheSession = new ConcurrentHashMap<String, Session>();

    public CacheSessionManager() {
        super();
    }

    public CacheSessionManager(SessionDao sessionDao) {
        super(sessionDao);
    }

    public CacheSessionManager(SessionIdGenerator sessionIdGenerator) {
        super(sessionIdGenerator);
    }

    public CacheSessionManager(SessionIdGenerator sessionIdGenerator, SessionDao sessionDao) {
        super(sessionIdGenerator, sessionDao);
    }

    public CacheSessionManager(SessionDao sessionDao, SessionIdGenerator sessionIdGenerator) {
        super(sessionDao, sessionIdGenerator);
    }

    @Override
    public void addSession(Session session) {
        if (session == null) return;
        cacheSession.put(session.getId(), session);
        super.addSession(session);
    }

    @Override
    public void removeSession(String sessionId) {
        if (StrKit.isBlank(sessionId)) return;
        cacheSession.remove(sessionId);
        super.removeSession(sessionId);
    }

    @Override
    public Session getSession(String sessionId) {
        if (StrKit.isBlank(sessionId)) return null;
        Session session = cacheSession.get(sessionId);
        if (session == null) {
            session = super.getSession(sessionId);
            if (session != null) {
                cacheSession.put(session.getId(), session);
            }
        } else {
            if (session.isInvalidate()) {
                removeSession(sessionId);
                return null;
            }
            session.setLastAccessedTime(System.currentTimeMillis());
            super.refreshSession(session);
        }
        return session;
    }
}
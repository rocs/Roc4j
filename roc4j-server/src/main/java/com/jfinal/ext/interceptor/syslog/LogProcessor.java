/**
 * Copyright (c) 2011-2013, kidzhou 周磊 (zhouleib1412@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jfinal.ext.interceptor.syslog;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import com.jfinal.core.Controller;

/**
 * 日志处理器
 */
public interface LogProcessor {

    /**
     * 处理一条日志
     * @param sysLog
     */
    void process(SysLog sysLog);

    /**
     * 获取当前登陆用户名
     * @param c
     * @return
     */
    String getUsername(Controller c);

    /**
     * 格式化日志信息
     * @param title
     * @param message
     * @return
     */
    String formatMessage(String title, Map<String, String> message);
}

/**
 * Copyright (c) 2011-2016, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfinal.log;

import org.apache.log4j.Level;

import java.text.MessageFormat;

/**
 * Log4jLog.
 */
public class Log4jLog extends Log {
	
	private org.apache.log4j.Logger log;
	private static final String callerFQCN = Log4jLog.class.getName();
	
	Log4jLog(Class<?> clazz) {
		log = org.apache.log4j.Logger.getLogger(clazz);
	}
	
	Log4jLog(String name) {
		log = org.apache.log4j.Logger.getLogger(name);
	}
	
	public static Log4jLog getLog(Class<?> clazz) {
		return new Log4jLog(clazz);
	}
	
	public static Log4jLog getLog(String name) {
		return new Log4jLog(name);
	}
	
	public void info(String message) {
		log.log(callerFQCN, Level.INFO, message, null);
	}

	@Override
	public void info(String message, Object... params) {
		log.log(callerFQCN, Level.INFO, MessageFormat.format(message, params), null);
	}

	public void info(String message, Throwable t) {
		log.log(callerFQCN, Level.INFO, message, t);
	}

	@Override
	public void info(String message, Throwable t, Object... params) {
		log.log(callerFQCN, Level.INFO, MessageFormat.format(message, params), t);
	}

	public void debug(String message) {
		log.log(callerFQCN, Level.DEBUG, message, null);
	}

	@Override
	public void debug(String message, Object... params) {
		log.log(callerFQCN, Level.DEBUG, MessageFormat.format(message, params), null);
	}

	public void debug(String message, Throwable t) {
		log.log(callerFQCN, Level.DEBUG, message, t);
	}

	@Override
	public void debug(String message, Throwable t, Object... params) {
		log.log(callerFQCN, Level.DEBUG, MessageFormat.format(message, params), t);
	}

	public void warn(String message) {
		log.log(callerFQCN, Level.WARN, message, null);
	}

	@Override
	public void warn(String message, Object... params) {
		log.log(callerFQCN, Level.WARN, MessageFormat.format(message, params), null);
	}

	public void warn(String message, Throwable t) {
		log.log(callerFQCN, Level.WARN, message, t);
	}

	@Override
	public void warn(String message, Throwable t, Object... params) {
		log.log(callerFQCN, Level.WARN, MessageFormat.format(message, params), t);
	}

	public void error(String message) {
		log.log(callerFQCN, Level.ERROR, message, null);
	}

	@Override
	public void error(String message, Object... params) {
		log.log(callerFQCN, Level.ERROR, MessageFormat.format(message, params), null);
	}

	public void error(String message, Throwable t) {
		log.log(callerFQCN, Level.ERROR, message, t);
	}

	@Override
	public void error(String message, Throwable t, Object... params) {
		log.log(callerFQCN, Level.ERROR, MessageFormat.format(message, params), t);
	}

	public void fatal(String message) {
		log.log(callerFQCN, Level.FATAL, message, null);
	}

	@Override
	public void fatal(String message, Object... params) {
		log.log(callerFQCN, Level.FATAL, MessageFormat.format(message, params), null);
	}

	public void fatal(String message, Throwable t) {
		log.log(callerFQCN, Level.FATAL, message, t);
	}

	@Override
	public void fatal(String message, Throwable t, Object... params) {
		log.log(callerFQCN, Level.FATAL, MessageFormat.format(message, params), t);
	}

	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}
	
	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}
	
	public boolean isWarnEnabled() {
		return log.isEnabledFor(Level.WARN);
	}
	
	public boolean isErrorEnabled() {
		return log.isEnabledFor(Level.ERROR);
	}
	
	public boolean isFatalEnabled() {
		return log.isEnabledFor(Level.FATAL);
	}
}


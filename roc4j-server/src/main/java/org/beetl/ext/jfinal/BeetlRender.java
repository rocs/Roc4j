/*
[The "BSD license"]
Copyright (c) 2011-2013 Joel Li (李家智)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package org.beetl.ext.jfinal;

import org.beetl.core.GroupTemplate;
import org.beetl.core.exception.BeetlException;
import org.beetl.ext.web.WebRender;

import com.jfinal.render.Render;
import com.jfinal.render.RenderException;

public class BeetlRender extends Render
{
	GroupTemplate gt = null;
	private transient static final String encoding = getEncoding();
	private transient static final String contentType = "text/html; charset=" + encoding;

	public static final String KEY_RENDER_TIME = "RENDER_TIME";

	public BeetlRender(GroupTemplate gt, String view)
	{
		this.gt = gt;
		this.view = view;
	}

	public BeetlRender(String view) {
		this(BeetlRenderFactory.me.groupTemplate, view);
	}

	/**
	 * 继承类可通过覆盖此方法改变 contentType，从而重用 beetl 模板功能
	 * 例如利用 beetl 实现 BeetlRender 生成 Xml 内容
	 */
	public String getContentType() {
		return contentType;
	}

	@Override
	public void render()
	{
		long start = System.currentTimeMillis();
		try
		{
			response.setContentType(contentType);
			WebRender webRender = new WebRender(gt);
			webRender.render(view, request, response);
		}
		catch (BeetlException e)
		{
			throw new RenderException(e);
		}
		request.setAttribute(BeetlRender.KEY_RENDER_TIME, System.currentTimeMillis() - start);
	}

}
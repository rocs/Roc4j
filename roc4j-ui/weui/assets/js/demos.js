/**
 * Created by peng on 2016/5/9.
 */
$(function () {
    var $dark = $("#dark-switch").on("change", function () {
        $(document.body)[$dark.is(":checked") ? "addClass" : "removeClass"]("theme-dark");
    });
    //显示一个对话框
    $.alert("Hello WeUI！");
});
